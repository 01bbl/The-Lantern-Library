/*jshint esversion: 6*/

/* ==================
	TRACK REVIEWS
================== */

// list of all track reviews, including medals and thumbs-up/down ratings, split by cup
let trackReviews = {
"ring-cup": [
	{
	"name": `Robotnik Coaster`,
	"rating": 0,
	"medal": 3,
	"review": `When I compare this to <cite>Sonic Kart</cite>'s Green Hills Zone as the first tracks in their respective games, the latter comes out on top. Where the original Green Hills Zone feels like it's meant to ease people into the game without being trivial, this feels like it's meant to frustrate people by pinballing them around the track. Saved from thumblessness by its overall aesthetic. <strong>Update:</strong> The longer and longer I play the game the worse the first part of this track feels, so I'm cutting its last thumb off.`,
	},
	{
	"name": `Northern District`,
	"rating": 1,
	"medal": 4,
	"review": `Aesthetically an improvement on <cite>Sonic Kart</cite>'s version, with the shift from a completely undifferentiated colour palette to very intentional use of colours, though I feel like they lost something when they cut some of the minor branching paths. <strong>Update:</strong> Okay it's actually pretty good, and the girder shortcut's rad if you can you get it.`,
	},
	{
	"name": `Panic City`,
	"rating": 0,
	"medal": 3,
	"review": `Weirdly early in the game for how tricky some of its turns can be, and now that I know the game it's just kinda nothing.`,
	},
	{
	"name": `Sonic Speedway`,
	"rating": 0,
	"medal": 4,
	"review": `A decent early track with some awkwardly-spaced pairs of turns that don't feel right to drift straight through. Visually it's definitely an upgrade from the <cite>Sonic Kart</cite> version, much better differentiated from the more realistic colour scheme of Daytona Speedway.`,
	},
	{
	"name": `Green Hills Zone`,
	"rating": 1,
	"medal": 4,
	"review": `The first big change from a vanilla or modded <cite>Sonic Kart</cite> track, and definitely the biggest challenge in the first cup, with some tricky (for beginners) sloped turns.`,
	},
],
"sneaker-cup": [
	{
	"name": `Emerald Coast`,
	"rating": 1,
	"medal": 4,
	"review": `Vibrant and fun track design marred by that one extremely annoying spring spam section on the first boardwalk&mdash;the first of many in the game. Other than that, it's also a pretty good first brush with water phyics, with basically only one challenge (a single right-angled turn leading into a straightaway). Finally, that water section has some cool vertical branching that's gonna show up again in later tracks.`,
	},
	{
	"name": `Storm Rig`,
	"rating": 0,
	"medal": 3,
	"review": `Feels like a training track for learning to drift well, knowing when to release and where to sliptide. Otherwise very simple, but it does have one of the better Ancient Shrines.`,
	},
	{
	"name": `Lucid Pass`,
	"rating": 2,
	"medal": 3,
	"review": `Maybe the most <cite>Sonic Kart</cite> a map has been so far, and a step up from Storm Rig as an advanced introduction to drifting (it reminds me of Darkvile Garden from <cite>Sonic Kart</cite> in how you can drift through almost the entire track). I feel like holding a sliptide from the map's only boost pad to the end of the lap should unlock something&hellip;`,
	},
	{
	"name": `Autumn Ring`,
	"rating": 2,
	"medal": 4,
	"review": `What if Lucid Pass was more <cite>Ring Racers</cite>?`,
	},
	{
	"name": `Withering Chateau`,
	"rating": 1,
	"medal": 4,
	"review": `An all-round solid track.`,
	},
],
"spring-cup": [
	{
	"name": `Popcorn Workshop`,
	"rating": 1,
	"medal": 3,
	"review": `A straightforward track with some basic interactivity and a fun theme and tune.`,
	},
	{
	"name": `Sundae Drive`,
	"rating": 2,
	"medal": 3,
	"review": `In some way it feels more like this game's version of <cite>Sonic Kart</cite>'s Daytona Speedway than this game's actual Daytona Speedway.`,
	},
	{
	"name": `Cadillac Cascade`,
	"rating": 0,
	"medal": 3,
	"review": `Some great views of the track ahead and music that perfectly matches the slow, wide turns and big expanses, but it doesn't feel satisfying to race on.`,
	},
	{
	"name": `Rumble Ridge`,
	"rating": 2,
	"medal": 4,
	"review": `The first truly hype track of the game, between the music, aesthetic, and the sweeping drifts approaching the giant machine.`,
	},
	{
	"name": `Opulence Zone`,
	"rating": 1,
	"medal": 3,
	"review": `The arc full of boost pads is a great way to learn sliptiding and the map overall feels more &ldquo;opulent&rdquo; than the <cite>Sonic Kart</cite> version, but I really don't like the hitboxes on the massive spiked balls. This comes up time and again, but here's the first place I really, painfully noticed it: many entity/hazard hitboxes feel significantly bigger to me than the actual graphics.`,
	},
],
"barrier-cup": [
	{
	"name": `Angel Island`,
	"rating": 0,
	"medal": 4,
	"review": `A few hard-to-read areas in an overall-fine track. I feel like, given how iconic the original Sonic 3 level is, there could've been a transition where Eggman's robots start to burn the forest down, but ah well.`,
	},
	{
	"name": `Roasted Ruins`,
	"rating": 1,
	"medal": 3,
	"review": `The music has so much more energy than the <cite>Sonic Kart</cite> version and it's so satisfying to snag all the boost pads on the opening hill, and generally it feels alright to take on this entire drift-heavy track.`,
	},
	{
	"name": `Obsidian Oasis`,
	"rating": -2,
	"medal": 3,
	"review": `I didn't really like the original <cite>Sonic Kart</cite> mod map, but this one's worse. Too much of it's just just barely-navigable visual chaos to me.`,
	},
	{
	"name": `Mirage Saloon`,
	"rating": 0,
	"medal": 4,
	"review": `I feel like the track design could do with being slightly more relaxed, to go with the lower-key music (or vice versa bring the musical energy up a bit).`,
	},
	{
	"name": `Regal Ruin`,
	"rating": 1,
	"medal": 3,
	"review": `There's a neat idea at the start of this track, with the wide open area, but I feel like they don't do enough with it to make it worth doing anything but cutting straight across most of the time. I like the messy branching paths, though.`,
	},
],
"invincible-cup": [
	{
	"name": `Isolated Island`,
	"rating": -1,
	"medal": 4,
	"review": `I don't have anything to say about this level.`,
	},
	{
	"name": `Gigapolis`,
	"rating": 0,
	"medal": 4,
	"review": `This track could really stand to be scaled down and 5 laps long&hellip;`,
	},
	{
	"name": `Darkvile Castle 1`,
	"rating": 0,
	"medal": 3,
	"review": `I always felt Darkvile Garden was a great early track with a terrible aesthetic, and this version kinda evens things out a bit.`,
	},
	{
	"name": `Bronze Lake`,
	"rating": 1,
	"medal": 4,
	"review": `An almost meditative high-speed track with appropriately lower-key music.`,
	},
	{
	"name": `Collision Chaos`,
	"rating": 1,
	"medal": 4,
	"review": `One thing this level really has going for it is the music. Instead of some grating casino track it's cool, collected, and a little bit tense, perfect for a track where you can elegantly weave between all the bounce hazards or just smack right into them.`,
	},
],
"emerald-cup": [
	{
	"name": `Emerald Hill Zone`,
	"rating": 1,
	"medal": 4,
	"review": `Honestly feels like more of an update to <cite>Sonic Kart</cite>'s Green Hills Zone than the actual Green Hills Zone track in this game.`,
	},
	{
	"name": `Azure City`,
	"rating": 1,
	"medal": 3,
	"review": `If only there were more streets&hellip;`,
	},
	{
	"name": `Gust Planet`,
	"rating": 0,
	"medal": 3,
	"review": `A surprisingly basic high-speed rush.`,
	},
	{
	"name": `Mystic Cave`,
	"rating": 1,
	"medal": 4,
	"review": `Much better than the only Mystic Cave track I remember from <cite>Sonic Kart</cite> (a modded track). The crusher-block challenges are stressful in the right way.`,
	},
	{
	"name": `Joypolis`,
	"rating": 2,
	"medal": 3,
	"review": `Maybe a little picky, but the springs after the helix feel a bit harsh on new players, which feels a little wrong for a track so vivid and iconic (iconic enough for the devs to feature it prominently in the <a href="https://www.youtube.com/watch?v=KvAJCrdhrrQ" rel="external">release trailer</a>).`,
	},
],
"extra-cup": [
	{
	"name": `Hilltop Zone`,
	"rating": 0,
	"medal": 4,
	"review": `Worse than the <cite>Sonic Kart</cite> modded track for two main reasons: the mean-spirited spring walls in the tunnel after the lift, and hard-to-pin-down slope behaviours after the final booster that too-often give me airtime when I try to avoid exactly that. Still, I always liked the part with three branches.`,
	},
	{
	"name": `Marble Garden Zone`,
	"rating": -2,
	"medal": 3,
	"review": `I want to like this, I really do. A more obstacle-course like track that can work as an advanced spindash tutorial (hell, a track that gives the spindash a positive purpose instead of just &ldquo;the thing I do at the start or when I get fucked up&rdquo;)? It sounds really interesting, and honestly, the one open uphill bit that people hate is my favourite part. It's everything else that's the problem&mdash;claustrophobic meandering tunnels and paths, full of hazards. They're annoying in races, but really awful in time trials, since the hazards don't reset when you restart the trial (unless you completely exit).`,
	},
	{
	"name": `Silvercloud Island`,
	"rating": 2,
	"medal": 4,
	"review": `The original <cite>Sonic Kart</cite> track was one of my favourites, partly for the style and partly because of the two wholly skill-based shortcuts in the first half. This version unfortunately lacks those shortcuts and also has been <em>heavily</em> colour-filtered to just blues and purples for the most part, and the buildings just kinda blend in with the landscape (I only realised there were still buildings on the sidelines while re-running the track as I wrote this). Still, it's a strong course and the music's just as hype.`,
	},
	{
	"name": `Sub-Zero Peak`,
	"rating": 1,
	"medal": 3,
	"review": `A reasonably-challenging pure ice track that shows ice can be an interesting hazard in itself and doesn't have to be paired with other bullshit.`,
	},
	{
	"name": `Launch Base`,
	"rating": 1,
	"medal": 4,
	"review": `The music's a little underwhelming for what is, at least temporarily, the last track of the game, but everything else works well and the map overall feels vast in scale.`,
	},
],
"spb-cup": [
	{
	"name": `Azure Lake`,
	"rating": 0,
	"medal": 3,
	"review": `Lots of fine drifting in the first half, but an awkward descent from the jump that never feels right to me (also, it feels a lot like you should be able to drive out from falling into the gap, but you'll almost always sink in the lake and have to respawn).`,
	},
	{
	"name": `Balloon Park`,
	"rating": -1,
	"medal": 3,
	"review": `More-or-less fine with the scale increased and collision fixed. Zero thumbs because I still don't like it. <strong>Update:</strong> Dropping to a negative rating because the funnel has regular slope physics&mdash;there are several places in this game where you can drive up <em>very</em> steep slopes like this that you should really just slide down, but this might be the worst.`,
	},
	{
	"name": `Chrome Gadget`,
	"rating": -1,
	"medal": 3,
	"review": `Why is this level so goddamn dark?`,
	},
	{
	"name": `Desert Palace`,
	"rating": 1,
	"medal": 4,
	"review": `A strong small track back from <cite>Sonic Kart</cite>, but I feel like it has a slower pace now.`,
	},
	{
	"name": `Endless Mine 1`,
	"rating": 2,
	"medal": 4,
	"review": `Sometimes all you need is a really, really big helix.`,
	},
],
"rocket-cup": [	
	{
	"name": `Hard-Boiled Stadium`,
	"rating": 1,
	"medal": 3,
	"review": `This track kinda grew on me. Lots of hazards and enough forewarning to react properly if you're on the ball (mostly&hellip; those bombs on the ice still get me).`,
	},
	{
	"name": `Hardhat Havoc`,
	"rating": 1,
	"medal": 4,
	"review": `A good, if simple, rally course.`,
	},
	{
	"name": `Press Garden`,
	"rating": 1,
	"medal": 3,
	"review": `A solid track carried higher by its style.`,
	},
	{
	"name": `Pico Park`,
	"rating": 1,
	"medal": 3,
	"review": `For that special &ldquo;flaming box of grenades&rdquo; experience (in multiplayer, anyway).`,
	},
	{
	"name": `City Escape`,
	"rating": 1,
	"medal": 3,
	"review": `It's not the crate walls that bother me&mdash;I think they actually create a sense of checkpoints between separate kinds of track segment&mdash;but the narrow, slippery staircases can get frustrating fast with how you can pinball around them. That aspect really doesn't compare well with the equivalent bits of the original <cite>Sonic Adventure 2</cite> level.`,
	},
],
"aqua-cup": [	
	{
	"name": `Palmtree Panic`,
	"rating": 0,
	"medal": 4,
	"review": `It doesn't really stand out&mdash;just feels like a variation on Emerald Hill. If that's intentional because both are stereotypical green, hilly, forested first levels of Sonic side-scrollers then it's a bit clever, I guess, but maybe not worth making a whole track over.`,
	},
	{
	"name": `Darkvile Castle 2`,
	"rating": 0,
	"medal": 3,
	"review": `Some awkwardly tight or sudden corners, but overall a reasonable track. The only problem is that I just don't like it.`,
	},
	{
	"name": `Scarlet Gardens`,
	"rating": 0,
	"medal": 4,
	"review": `Can be frustrating in multiplayer due to the winding course design and single item box set, but it's pulled up a lot by the powerful beat of the music and lots of small, but significant choices of whether to cut across offroad of stay on-road.`,
	},
	{
	"name": `Motobug Motorway`,
	"rating": 1,
	"medal": 4,
	"review": `A solid track from Ivo Industries, tightened up and with some neat new touches like the night-time look and fireworks. The one thing I'm not a big fan of is the new tunnel ending (I preferred the cave cliffside road more from the original). Also, the speedbumps, specifically their edges, are some of the worst offenders for geometry that has wildly unpredictable physics (am I gonna fly into the air or drive straight over? Who knows).`,
	},
	{
	"name": `Star Light Zone`,
	"rating": 1,
	"medal": 3,
	"review": `Alternates blistering speeds with some lightly tricky drifting with minimal annoying offroad.`,
	},
],
"lightning-cup": [
	{
	"name": `Metropolis Zone`,
	"rating": -1,
	"medal": 4,
	"review": `I just find all the periodic traps with 0 indicators kinda annoying. <strong>Update:</strong> Yeah I've gotta say this track doesn't got it; I'm taking its rating down 1.`,
	},
	{
	"name": `Frozen Production`,
	"rating": -2,
	"medal": 4,
	"review": `The ice jet mechanic's more annoying than interesting to me. <strong>Update:</strong> I'm starting to think whether the jets freeze you's somehow random because of how inconsistent they are&mdash;sometimes I can safely drive straight through a few of the vertical ones, only to get caught far outside another jet's sprites.`,
	},
	{
	"name": `Aqueduct Crystal`,
	"rating": 0,
	"medal": 3,
	"review": `I like the shallow-water track idea&mdash;it's a neat way to ease people into water physics (since you gotta race pretty much the whole track with them, but don't always have the wavering effect on the graphics). It also makes for an interesting, if rare, choice of whether to drive on-road with water physics or off-road without. I just wish they hadn't gotten rid of the techno-temple flying island backdrop. <strong>Update:</strong> On reflection, this level kinda fucking sucks. I'm giving it a completely neutral rating now because it's not that bad in multiplayer, but it's <em>horrendous</em> when going for platinum in time trials due to the shallow water and roadside slopes making it extremely easy to get launched and skip pointlessly across the water's surface (this kills the run).`,
	},
	{
	"name": `Nova Shore`,
	"rating": 1,
	"medal": 3,
	"review": `Visually it's good, but, for seemingly no reason, you can't bounce on the pools of water beside the track. They're just death pits, basically. It's weird, because they're very obvious shortcuts (if you have the speed and are willing to lose a bit of that speed to cut the corner).`,
	},
	{
	"name": `Hydro City`,
	"rating": -1,
	"medal": 4,
	"review": `Awkwardly-proportioned.`,
	},
],
"flame-cup": [	
	{
	"name": `Trap Tower`,
	"rating": 0,
	"medal": 3,
	"review": `A little frustrating, but I guess that's the point, and it helps that it has fun music.`,
	},
	{
	"name": `Diamond Dust Zone`,
	"rating": -2,
	"medal": 3,
	"review": `Probably the worst ice level in the game, with a kitchen sink approach to hazards that become incredibly annoying in this track (tumbling snowmen, huge bumpers, freezing jets).`,
	},
	{
	"name": `Blue Mountain 1`,
	"rating": 2,
	"medal": 3,
	"review": `Great high-speed ice level aside from the slightly-tight slalom section at the end. <strong>Update:</strong> Once you get the hang of the slalom section this track's even better so I'm pushing its rating up by another thumb.`,
	},
	{
	"name": `Blue Mountain 2`,
	"rating": 2,
	"medal": 3,
	"review": `Very hype all round, dragged down by the snowman hazards. I feel like the concept (high-speed chase on icy cliffside roads as night turns to dawn) can stand by itself without them.`,
	},
	{
	"name": `Speed Highway`,
	"rating": -1,
	"medal": 3,
	"review": `It's hobbled by clunky design that often stops you from actually moving at high speeds unless you know the course very well, and sometimes even that can't help (e.g. the bridge that blocks you from voltage-dropping near the end). The final straightaway's pretty cool, but the easiest and best way to handle it's just to go to the edge rather than actually engage with the hazards (which are kind of a pain to deal with).`,
	},
],
"super-cup": [	
	{
	"name": `Carnival Night Zone`,
	"rating": -2,
	"medal": 2,
	"review": `You knew this was coming. A map with awkward and sometimes unpredictable physics elements that feel half-baked, combined with sudden hazards, obnoxious spike positions, and ultra-precise platforming elements that dunk you in frustrating, speed-hobbling underwater sections&hellip; It'd be more interesting if you were meant to take it at a more deliberate pace, but the time trials betray that you're &ldquo;supposed&rdquo; to drive it at blistering speeds. The coveted rank of &ldquo;worst part of worst course&rdquo; is a tough to award in such a crowded and competitive field, but I think it's gotta go to the stunningly-awful physics in the poles section at the start.`,
	},
	{
	"name": `Virtual Highway`,
	"rating": 1,
	"medal": 3,
	"review": `It's good that they made the fire jets an actual hazard and not just something you might hit once in a blue moon as in the original <cite>Sonic Kart</cite> track, and visually it's a lot more modern of an approach to the idea of a virtual world, but other than that I feel like the course changes were for the worse, particularly the music.`,
	},
	{
	"name": `Dark Fortress`,
	"rating": 0,
	"medal": 3,
	"review": `On the one hand, there are cool bits like the sliptide U-turns, big trick pad magma hallway near the start, and double trick pad near the end. On the other hand, there are bits like the wide, flat spiral staircase near the start that's inexplicably the rough-road terrain type. In conclusion, Dark Fortress is a land of contrasts.`,
	},
	{
	"name": `Spring Yard`,
	"rating": 0,
	"medal": 3,
	"review": `Kind of like Carnival Night Zone with less bullshit, aside from the final hazard (an unseeable, <em>moving</em> bumper right before the finish line). Saved a bit by its more compelling aesthetic and music.`,
	},
	{
	"name": `Labyrinth Zone`,
	"rating": 2,
	"medal": 4,
	"review": `Finally, some good fucking food. Branching and multi-layered, it lives up to the concept&mdash;like an improved version of Regal Ruin.`,
	},
],
"egg-cup": [	
	{
	"name": `Hot Shelter`,
	"rating": 2,
	"medal": 3,
	"review": `A rework of <cite>Sonic Kart</cite>'s Red Barrage Area that takes more inspiration from its source material (Sonic Adventure) and has tighter hazard-based challenges. If only they included Gamma's training area&hellip;`,
	},
	{
	"name": `Sky Sanctuary`,
	"rating": 0,
	"medal": 3,
	"review": `Alternates between seemingly incredibly dangerous and being incredibly safe, and vice versa. At least the music's good.`,
	},
	{
	"name": `Lost Colony`,
	"rating": 1,
	"medal": 3,
	"review": `I've seen some people criticise this level on the grounds that there are brighter or more spectacular Sonic Adventure 2 space levels they could've used, and while I get it, having a low-light track set in a cavernous space colony is fun and the outer space parts with the <span class="uppercase">Ark</span> dimly glowing in the background have an good amount of menace. What drags it down are a few awkward turns and the fact that the lift room at the end will almost never produce an interesting situation that makes the wait worth it (e.g. 2+ people being on the same lift).`,
	},
	{
	"name": `Death Egg`,
	"rating": -1,
	"medal": 3,
	"review": `Racing a final gauntlet through the guts of a colossal outer-space death machine shouldn't feel this boring. Specifically, it's the graphics, music, and lack of major changes between cycles that don't do it for me (the anti-gravity stuff's a pain, too). There's a lotta gauntlets among modded <cite>Sonic Kart</cite> tracks (Crumbling Tower, Final Tower, Burning Bone Zone, Golden Passage, Thousand March, Mille Marche, Spelunky&hellip;) so I'm surprised they didn't take inspiration from any of those. I will, however, pick this track in multiplayer any time it appears, until it's raced once per session.`,
	},
],
"goggles-cup": [	
	{
	"name": `765 Stadium`,
	"rating": 1,
	"medal": 3,
	"review": `A hype speed track, though I wish they'd kept the glowsticks under the jumps from the <cite>Sonic Kart</cite> version. <strong>Update:</strong> The more I play this track, the less I like the lack of item boxes. It makes it extremely hard to catch up in tight races.`,
	},
	{
	"name": `Skyscraper Leaps`,
	"rating": 2,
	"medal": 4,
	"review": `I feel like the textures or just the skybox could afford to be a little more spectacular but that's basically the only note. A tightly-designed course with perfect music and a great advanced tutorial for the triangle dash.`,
	},
	{
	"name": `Green Triangle`,
	"rating": 0,
	"medal": 3,
	"review": `It is what it is.`,
	},
	{
	"name": `Zoned City`,
	"rating": 1,
	"medal": 3,
	"review": `Whee&hellip;!`,
	},
	{
	"name": `Sunset Hill`,
	"rating": 0,
	"medal": 3,
	"review": `A nice laid-back track whose only real flaw is the first jump over the rising boxes, where, in my experience, it's almost impossible to clear the fence if you dodge the boxes (even on Gear 3).`,
	},
],
"timer-cup": [
	{
	"name": `Savannah Citadel`,
	"rating": 1,
	"medal": 4,
	"review": `A fun chunky course with just the right distribution of slopes.`,
	},
	{
	"name": `Umbrella Rushlands`,
	"rating": -2,
	"medal": 3,
	"review": `I never really liked the original <cite>Sonic Kart</cite> track, and I'm not surprised I didn't like this one either. The part just before the helix is probably the worst, since there's so little distinction between road and wall.`,
	},
	{
	"name": `Avant Garden`,
	"rating": 0,
	"medal": 3,
	"review": `Surprisingly basic.`,
	},
	{
	"name": `Bigtime Breakdown`,
	"rating": 1,
	"medal": 3,
	"review": `The big turntables add the right amount uncertainty and chaos; the music could've been more aggressive or present for a music-themed course, though.`,
	},
	{
	"name": `Vantablack Violet`,
	"rating": 2,
	"medal": 3,
	"review": `The aesthetic isn't my thing, but I do appreciate a good hype track, and this one has enough snags to be a challenge.`,
	},
],
"grow-cup": [
	{
	"name": `Chaos Chute`,
	"rating": 0,
	"medal": 3,
	"review": `Thank God they added some hazards to spice this up.`,
	},
	{
	"name": `Dimension Disaster`,
	"rating": 2,
	"medal": 3,
	"review": `A challengingly narrow course with meaningful choices for using offroad, and an aggressive musical background and spectacular look.`,
	},
	{
	"name": `Aurora Atoll`,
	"rating": 0,
	"medal": 3,
	"review": `Very changed from its <cite>Sonic Kart</cite> form, honestly neither for the better or the worse (relative to each game).`,
	},
	{
	"name": `Daytona Speedway`,
	"rating": 1,
	"medal": 3,
	"review": `It's just not the same&hellip;`,
	},
	{
	"name": `Turquoise Hill Zone`,
	"rating": 1,
	"medal": 3,
	"review": `The music feels a little clunky sometimes, but it can't drag down this tricky drift-heavy track that honestly does feel better to drive than the <cite>Sonic Kart</cite> mod track by the same name.`,
	},
],
"chao-cup": [
	{
	"name": `Weiss Waterway`,
	"rating": 1,
	"medal": 4,
	"review": `A relatively straightforward track with an interesting challenge in the underwater offroad section.`,
	},
	{
	"name": `Ice Paradise`,
	"rating": 0,
	"medal": 4,
	"review": `A solid ice track with no extra gimmicks, but it doesn't really grab me the way some other ice tracks do.`,
	},
	{
	"name": `Sunsplashed Getaway`,
	"rating": -2,
	"medal": 4,
	"review": `The massive amounts of offroad causes constant stop-start movement and rumbling/grinding sounds that make this track really dismal to play.`,
	},
	{
	"name": `Fae Falls`,
	"rating": 0,
	"medal": 3,
	"review": `A decent track hampered by some weird lighting choices (e.g. near the first cave and at the tripwire in the last cave) that make the course harder to read.`,
	},
	{
	"name": `Azure Axiom`,
	"rating": 2,
	"medal": 3,
	"review": `A strong water track that forces you to get good at the alternate physics. I feel like they could've gone somewhere with the idea of a massive multi-checkpoint circuit (e.g. 4 checkpoints on a 3-checkpoint lap), but it's really just a big sprint (unless you edit the track config, I guess). Side note, was there a pervert for the word &ldquo;azure&rdquo; on the dev team? Three levels with that in the name&hellip;`,
	},
],
"wing-cup": [
	{
	"name": `Hanagumi Hall`,
	"rating": 1,
	"medal": 3,
	"review": `A neat compact techical track. I don't have much more to say other than that the secret hallway feels incomplete, like it was meant to be full of references instead of mostly-empty rooms.`,
	},
	{
	"name": `Aerial Highlands`,
	"rating": -1,
	"medal": 3,
	"review": `Out of all the tracks in the game, this one feels maybe the most like a <cite>Sonic Kart</cite> map, in a bad way.`,
	},
	{
	"name": `Crispy Canyon`,
	"rating": 2,
	"medal": 3,
	"review": `Mirage Saloon wishes it had Crispy Canyon's swagger.`,
	},
	{
	"name": `Technology Tundra`,
	"rating": 2,
	"medal": 3,
	"review": `A good track carried in a major way by its music, but I'm just glad there are more non-gimmick ice tracks in this game&mdash;tracks that treat ice as a worthy hazard in itself.`,
	},
	{
	"name": `Operator's Overspace`,
	"rating": 1,
	"medal": 4,
	"review": `A very hype track that unfortunately doesn't have the challenge to really excel.`,
	},
],
"mega-cup": [	
	{
	"name": `Mega Green Hill Zone`,
	"rating": 1,
	"medal": 3,
	"review": `Some tricky drifting&mdash;one of the type where I think as I get better at the track I'll get better at the game.`,
	},
	{
	"name": `Mega Bridge Zone`,
	"rating": 1,
	"medal": 4,
	"review": `A cute track that pulls off the heavy on/offroad mix a lot better than Sunsplashed Getaway.`,
	},
	{
	"name": `Mega Lava Reef`,
	"rating": 0,
	"medal": 3,
	"review": `I'm glad you're given enough fore-warning of the hazards that it's not basically a gamble as to whether you get hit (unlike certain other tracks).`,
	},
	{
	"name": `Mega Ice Cap`,
	"rating": 0,
	"medal": 4,
	"review": `Nowhere near as bad as Vanilla Lake. The ice blocks are spread out more and each group poses its own problem instead of most of them being in one main dense bullshit block.`,
	},
	{
	"name": `Mega Scrap Brain`,
	"rating": 2,
	"medal": 3,
	"review": `I don't like it, but I have to respect it. Anyway, this is maybe the best example of a spiritual successor in the Mega Cup, with a track design that clearly takes elements of the original (thwomps, narrow lanes, lap-by-lap changes) and recasts them in a much better, but still recognisable form.`,
	},
],
"phantom-cup": [	
	{
	"name": `Wavecrash Dimension 1`,
	"rating": 0,
	"medal": 4,
	"review": `I feel like they could add a bit more hydroplaning, but it's a major step up from the retro recreation in <cite>Sonic Kart</cite>.`,
	},
	{
	"name": `Nightfall Dimension 2`,
	"rating": 1,
	"medal": 4,
	"review": `Probably the most interesting layout of these stages, but I feel like they could've gone weirder and crazier with the various sprites.`,
	},
	{
	"name": `Voiddance Dimension 3`,
	"rating": 0,
	"medal": 3,
	"review": `I like the fact that you can shoot projectiles through the wall in the middle of the snaking double-back at the end of the lap, to put the fear of god into the front-runners.`,
	},
	{
	"name": `Cloudtop Dimension 4`,
	"rating": 1,
	"medal": 3,
	"review": `The much wider layout and sparser hazards let you build up some incredible speed, but that aside I don't have much to say.`,
	},
	{
	"name": `Gravtech Dimension 5`,
	"rating": -1,
	"medal": 3,
	"review": `The aesthetic doesn't do much for me, but at least the anti-grav parts are light-weight. The gimmick is interesting, but I wish they did at least one more noteworthy thing with it beyond how it's used in positioning and the boost pad island section.`,
	},
],
"flash-cup": [	
	{
	"name": `Espresso Lane`,
	"rating": 2,
	"medal": 4,
	"review": `Cute aesthetic and uncommon food theme, plus it's packed with challenging corners. I wasn't expecting to like this one so much until I started replaying it.`,
	},
	{
	"name": `Melty Manor`,
	"rating": 0,
	"medal": 3,
	"review": `An at-times simple drift-heavy track with a lotta challenge potential if you try to hit those boost pads on the corners. The aesthetic doesn't really do it for me, though, and the music doesn't feel like it matches the look.`,
	},
	{
	"name": `Leaf Storm Zone`,
	"rating": 1,
	"medal": 4,
	"review": `It has <em>so</em> much potential that they waste by cutting it so short. I can't even say &ldquo;I only wish there was more of it&rdquo;, because that makes it sound like there was a decent amount to begin with. <strong>Update:</strong> Fine, I guess I'll raise the rating by 1.`,
	},
	{
	"name": `Lake Margorite`,
	"rating": 1,
	"medal": 3,
	"review": `Overall a definite improvement on navigability over the various <cite>Sonic Kart</cite> versions of this track. <strong>Update:</strong> It's not so bad, so I'm giving this one a thumb.`,
	},
	{
	"name": `Endless Mine 2`,
	"rating": 2,
	"medal": 4,
	"review": `Hog's Bother <strong>Update:</strong> Hog's Bother`,
	},
],
"swap-cup": [	
	{
	"name": `Cyan Belltower`,
	"rating": -1,
	"medal": 3,
	"review": `<cite>Sonic Kart</cite> fixed my wariness of right-angled corners in kart games and this track brought it back.`,
	},
	{
	"name": `Quartz Quadrant`,
	"rating": 0,
	"medal": 3,
	"review": `It has a cool gimmick&mdash;basically a more complex version of the interactivity in Popcorn Workshop&mdash;and some challenging turns, but the music's neither here nor there for me and I really don't like how the helix halfway through uses the same colour for the floor as it uses for the walls, ceiling, and the untouchable geometry across the death pits.`,
	},
	{
	"name": `Aqua Tunnel`,
	"rating": 0,
	"medal": 3,
	"review": `Really not sure what to make of this one, but at least I don't hate driving it.`,
	},
	{
	"name": `Water Palace`,
	"rating": 2,
	"medal": 4,
	"review": `A hype water track&hellip; they said it couldn't be done&hellip;`,
	},
	{
	"name": `Final Fall`,
	"rating": 1,
	"medal": 4,
	"review": `This has a really cool layout and it's one of the more platformer-y tracks that does that concept well; it's only let down by some bad signage near the end (that green down-arrow sign should probably show a hairpin or left-arrow instead).`,
	},
],
"shrink-cup": [	
	{
	"name": `Haunted Ship`,
	"rating": -2,
	"medal": 3,
	"review": `When taken at high speed it epitomises some of the worst pinball bullshit this game will throw at you, and the repeat quarter-pipe jumps really expose some of the flaws in the game's physics.`,
	},
	{
	"name": `Robotnik Winter`,
	"rating": 0,
	"medal": 4,
	"review": `I feel like they could've gotten more mileage out of the snowfall pits. Other than that, it just doesn't really grab me.`,
	},
	{
	"name": `Dragonspire Sewer 1`,
	"rating": 1,
	"medal": 4,
	"review": `Well-balanced, and it has solid music. A track where I really appreciate the slope physics being toned down so you don't go flying just from hitting the lower corners of the sewer pipe in the first section.`,
	},
	{
	"name": `Abyss Garden`,
	"rating": 2,
	"medal": 3,
	"review": `An excellent, challenging sprint track that aesthetically commits to one colour and does so much with it.`,
	},
	{
	"name": `Blizzard Peaks`,
	"rating": -1,
	"medal": 4,
	"review": `For some reason this hype track doesn't really do it for me. Two concrete problems: the trees have annoyingly-big hitboxes, and the path that returns you to the start of the circuit reads kind of odd, particularly with how narrow it is towards the end. <strong>Update:</strong> A third problem's that the upcoming track's so often hidden from view.`,
	},
],
"bomb-cup": [
	{
	"name": `Vermillion Vessel`,
	"rating": -1,
	"medal": 4,
	"review": `The scale of this map feels weird to me, like it's maybe 20% bigger than it should be in the second half.`,
	},
	{
	"name": `Dragonspire Sewer 2`,
	"rating": 2,
	"medal": 4,
	"review": `First a hype water track, now a hype sewer track?`,
	},
	{
	"name": `Chemical Facility`,
	"rating": 2,
	"medal": 4,
	"review": `Yes&hellip; Ha ha ha&hellip; Yes!`,
	},
	{
	"name": `Coastal Temple`,
	"rating": 0,
	"medal": 4,
	"review": `The waterside part of this track's <em>much</em> better in this version, wider allowing for more interesting play, but I'm not sure I like how heavily the map now features hanging foliage to block your sight in the tunnels. Maybe they could've varied it a bit in different parts of the track?`,
	},
	{
	"name": `Monkey Mall`,
	"rating": 2,
	"medal": 4,
	"review": `A straightforward no-bullshit challenge track whose slopes hide a little too much of the road ahead. <strong>Update:</strong> It's good chief&hellip;`,
	},
],
"power-cup": [	
	{
	"name": `Ramp Park`,
	"rating": -1,
	"medal": 4,
	"review": `Reminds me of the <cite>Sonic Kart</cite> mod maps Ridge Turnpike, Dynamite Wilderness, and Backroom Velocity Zone, but without the charm.`,
	},
	{
	"name": `Advent Angel Zone`,
	"rating": -1,
	"medal": 3,
	"review": `Frustrating as hell until you realise the best way to take this track is to avoid as many trick pads as possible, which is easier said than done when they cover most of the width of the track. Also, some of the pads <em>seem</em> to offer you a choice of different lanes depending on the direction you trick in, but trick inputs seem to be blocked for a fraction of a second after you hit the pad, which stops you from taking any route but forward. Finally, the time trial start position is impressively bad.`,
	},
	{
	"name": `Pestilence Zone`,
	"rating": 0,
	"medal": 4,
	"review": `Was there a sewer pervert on the dev team or something? In any case, this is definitely my least favourite of the sewer levels in the game, aside from the underwater (undersewage?) section. If they leant more into the tiny underwater ramp-jumps this level would've been better. Still, at least it has good style (the only thing keeping it from the Tommy No-Thumbs treatment).`,
	},
	{
	"name": `Crimson Core`,
	"rating": 2,
	"medal": 3,
	"review": `Gimmickier than its <cite>Sonic Kart</cite> form, but still a tricky late-game driving challenge, and it has appropriately maddeningly frenetic music. Everything hits right.`,
	},
	{
	"name": `Las Vegas`,
	"rating": 1,
	"medal": 4,
	"review": `Somehow doesn't grab me the way <cite>Sonic Kart</cite>'s Kart Airlines did (not that Kart Airlines was super-special). <strong>Update:</strong> I found the big shortcut&hellip; +1 thumb.`,
	},
],
"genesis-cup": [
	{
	"name": `Mega Collision Chaos`,
	"rating": 2,
	"medal": 3,
	"review": `A really good challenge for precise manoeuvring, and short enough that the bounce hazards don't outstay their welcome. Getting better at this track made me get better at the game.`,
	},
	{
	"name": `Mega Star Light Zone`,
	"rating": 1,
	"medal": 4,
	"review": `A nice relaxing track (and music) with some hair-raising death-pit-dodging corner cuts if you want to spice things up.`,
	},
	{
	"name": `Mega Sandopolis`,
	"rating": 0,
	"medal": 3,
	"review": `It just doesn't click with me. <strong>Update:</strong> Okay, it's not so bad.`,
	},
	{
	"name": `Mega Aqua Lake`,
	"rating": 2,
	"medal": 4,
	"review": `Another on/offroad super-shallow water track with a good mix of both without having an obnoxious amount of broken flow, and the water level gimmick's implemented well. Really satisfying to get perfect(-ish) laps on in time trial.`,
	},
	{
	"name": `Mega Flying Battery`,
	"rating": 0,
	"medal": 3,
	"review": `The only track where the E-brake actually has a particular purpose, as far as I can see. It'd be nice if the sides and edges of the track didn't use the same colours and mostly the same textures, but other than that this track is a good, fair challenge.`,
	},
],
"skate-cup": [	
	{
	"name": `Sky Babylon`,
	"rating": 2,
	"medal": 4,
	"review": `Hell yeah brother`,
	},
	{
	"name": `Kodachrome Void`,
	"rating": -1,
	"medal": 4,
	"review": `I'm glad they added back in animated textures vs the final version of this track from <cite>Sonic Kart</cite>, even if they're not super-messed-up, but they definitely made the more annoying parts of this track even more annoying (it's partly the physics, too, throwing you into the air at the slightest pebble or gripping you between the left/right corners in the hexagonal hallway). At least the music's good. <strong>Update:</strong> Not changing the rating, but this track is a <em>lot</em> better when you've got good handling.`,
	},
	{
	"name": `Lavender Shrine`,
	"rating": 2,
	"medal": 4,
	"review": `A charismatic track&hellip; I dunno what else to say.`,
	},
	{
	"name": `Thunder Piston`,
	"rating": 2,
	"medal": 3,
	"review": `Feels like a somewhat-easier Crystal Abyss (from <cite>Sonic Kart</cite>), which was one of my favourite tracks and the main way I tested new characters I made. In time trials, a reasonably hardcore pure driving challenge&mdash;just you against the track. <strong>Update:</strong> After playing this track online a lot with people who hate it I'm delighted to tell you it's still a banger.`,
	},
	{
	"name": `Dead Line`,
	"rating": -2,
	"medal": 3,
	"review": `Gimmicks and sucker-punches. It feels like a hell map, and I'm a little sad this was the last main track in the game and not, say, Thunder Piston, and conversely I don't think getting better at this track will help me get better at the game overall.`,
	},
],
"recycle-cup-a": [
	{
	"name": `SRB2 Frozen Night`,
	"rating": 0,
	"medal": 3,
	"review": `I'm glad they had some restraint and didn't just edit it into an ice version of Sunsplashed Getaway.`,
	},
	{
	"name": `Barren Badlands`,
	"rating": -1,
	"medal": 3,
	"review": `Feels too small with the faster speeds you can reach compared to <cite>Sonic Kart</cite>, and the blue badniks feel like they have awkwardly wider hitboxes.`,
	},
	{
	"name": `Shuffle Square`,
	"rating": -1,
	"medal": 3,
	"review": `The same problem as Obsidian Oasis: visual chaos, no sense of track hierarchy a lotta the time. Only gets a better rating because of the branching path fuckery.`,
	},
	{
	"name": `Blue Mountain Classic`,
	"rating": 0,
	"medal": 3,
	"review": `It doesn't really feel like anything special, but it's alright aside from the first two ice caves, which are just kinda awkward and hard to read.`,
	},
	{
	"name": `Angel Arrow Zone Classic`,
	"rating": 0,
	"medal": 3,
	"review": `Much more basic than its main-game counterpart, but without the flaws.`,
	},
],
"recycle-cup-b": [
	{
	"name": `Cadillac Canyon Classic`,
	"rating": 0,
	"medal": 3,
	"review": `It's pretty basic, but there's something neat about having 3 very similarly-shaped hairpins on the map that all feel different to drive through.`,
	},
	{
	"name": `Diamond Dust Classic`,
	"rating": 0,
	"medal": 3,
	"review": `Surprisingly tolerable compared to its main-game counterpart. The ice jets are minimised, the snowmen are just gone, and the bumpers are (mostly) out of the easier lines, so they can come into play through items, collisions, desperate gambles etc. They're maybe too lenient with the bumpers, though, but better that than too harsh.`,
	},
	{
	"name": `Blizzard Peaks Classic`,
	"rating": -2,
	"medal": 2,
	"review": `I want to like this, but combining the high-reflex U-turns and the hydroplaning section at the end with ice physics in a narrow track just makes it far too easy to pinball around. <strong>Update:</strong> Lavender Shrine Classic has a horrible design, but at least it feels intentional and complete. This is just a greasy smear of a level. I'm taking its rating all the way down.`,
	},
	{
	"name": `Launch Base Classic`,
	"rating": 0,
	"medal": 3,
	"review": `It lacks the sense of scale of the main-game version and the boost pad at the end of the underwater section is extremely jarring, as you immediately go from super-low to super-high steering sensitivity.`,
	},
	{
	"name": `Lavender Shrine Classic`,
	"rating": -2,
	"medal": 3,
	"review": `The spring walls are really obnoxious in this track's tight spaces&mdash;like Emerald Coast, but worse&mdash;and it's incredibly easy to get knocked off the track (despite this, offroad respawn triggers somehow feel <em>over</em>zealous).`,
	},
],
"lost-and-found": [
	{
	"name": `Test Track`,
	"rating": 0,
	"medal": 3,
	"review": `My main problem with this track is that it doesn't really feel like a <em>test</em>, since it's mostly made of big arcs with a few tight corners thrown in and has minimal extra features (no tricks, no underwater sections, etc.). Whether you're judging it in-story or out, it doesn't really function as a test of the game.`,
	},
	{
	"name": `Hidden Palace`,
	"rating": 1,
	"medal": 4,
	"review": `It's a cool track, but the conditions for finding it seem like the most interesting thing about it.`,
	},
],
};



/* ===============
	PARAMETERS
=============== */

// append new page elements
page.ratingLimitSelectorButtons = document.querySelector(`#rating-limit-selector-buttons`);
page.ratingSelectorButtons = document.querySelector(`#rating-selector-buttons`);
page.medalLimitSelectorButtons = document.querySelector(`#medal-limit-selector-buttons`);
page.medalSelectorButtons = document.querySelector(`#medal-selector-buttons`);
page.filteredTrackCount = document.querySelector(`#filtered-track-count`);



/* ==============
	FUNCTIONS
============== */

// hide filtered tracks based on current selectors and update filtered track count
function selectTracks() {
	const ratingLimit = page.ratingLimitSelectorButtons.querySelector('[aria-pressed="true"]').dataset.limit;
	const rating = parseInt(page.ratingSelectorButtons.querySelector('[aria-pressed="true"]').dataset.rating);
	const medalLimit = page.medalLimitSelectorButtons.querySelector('[aria-pressed="true"]').dataset.limit;
	const medal = parseInt(page.medalSelectorButtons.querySelector('[aria-pressed="true"]').dataset.medal);
	let n = page.trackReviews.length;

	for (const review of page.trackReviews) {
		const trackRating = parseInt(review.dataset.rating);
		const trackMedal = parseInt(review.dataset.medal);
		const hideTrack = (ratingLimit === `GTEQ` && trackRating < rating)
			|| (ratingLimit === `LTEQ` && trackRating > rating) 
			|| (ratingLimit === `EQ` && trackRating !== rating)
			|| (medalLimit === `GTEQ` && trackMedal < medal)
			|| (medalLimit === `LTEQ` && trackMedal > medal)
			|| (medalLimit === `EQ` && trackMedal !== medal);
		review.toggleAttribute(`hidden`, hideTrack);
		if (hideTrack) n -= 1;
	}

	page.filteredTrackCount.textContent = n;
}



/* ===========
	EVENTS
=========== */

document.addEventListener(`DOMContentLoaded`, () => {
	// build out track reviews on page
	const medals = ["no", "bronze", "silver", "gold", "platinum"];
	for (const [cup, tracks] of Object.entries(trackReviews)) {
		document.querySelector(`#${cup}-track-reviews`).innerHTML = tracks.map(track => `
		<div data-rating="${track.rating}" ${track.medal > 0 ? `data-medal="${track.medal}"` : ``}>
			<dt>${track.name}<span class="visually-hidden"> (${medals[track.medal]} medal)</span></dt>
			<dd>${track.review}</dd>
		</div>
		`).join(``);
	}
	trackReviews = null;

	// get all added track reviews
	page.trackReviews = document.querySelectorAll('[id$="-track-reviews"] > div');

	// add event listener for filtering tracks
	document.querySelectorAll('[id$="-selector-buttons"]').forEach(list => list.addEventListener(`click`, () => {
		if (event.target.tagName === `BUTTON` && !event.target.hasAttribute(`aria-disabled`)) {
			unpressButton(event.target.closest(`ul`).querySelector('[aria-pressed="true"]'));
			pressButton(event.target);
			selectTracks();
		};
	}));

	// initialise page
	document.querySelector(`#total-track-count`).textContent = page.trackReviews.length;
	selectTracks();
});
