/*jshint esversion: 11*/

// load in settings and initialise <body> classes
const settings = JSON.parse(window.localStorage.getItem(`settings`)) ?? {};
settings.theme ??= `light`;
settings.italics ??= `normal`;
settings.lines ??= `normal`;
for (const [setting, option] of Object.entries(settings)) document.documentElement.dataset[setting] = option;
