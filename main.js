/*jshint esversion: 11*/

/* ===============
	PARAMETERS
=============== */

const page = {
	// head
	"title": `title`,

	// sidebar
	"contents": `#contents`,

	// font guides
	"fontFeatureDemoButtons": `#font-feature-demo-buttons`,
	"fontLanguageDemoButtons": `#font-language-demo-buttons`,
	"customTextFontDemo": `#custom-text-font-demo`
};;

// build out page and templateHTML objects
for (const [ref, query] of Object.entries(page)) page[ref] = document.querySelector(query);

const fontDemoFeatures = new Set();



/* ==========
	INDEX
========== */

/* -----
SETTINGS
----- */

// update setting and buttons according to chosen value
function updateSetting(name, option) {
	unpressButton(page[`${name}Buttons`]?.querySelector(`[aria-pressed="true"]`));
	pressButton(page[`${name}Buttons`]?.querySelector(`[data-option="${option}"]`));

	document.documentElement.dataset[name] = option;
	settings[name] = option;
}

function buildSettingsButtons() {
	if (!document.querySelector(`#settings-buttons`)) return;

	for (const [name, value] of Object.entries(settings)) {
		page[`${name}Buttons`] = document.querySelector(`#${name}-buttons`);
		page[`${name}Buttons`].addEventListener(`click`, () => {
			if (event.target.tagName === `BUTTON` && !event.target.hasAttribute(`aria-disabled`)) updateSetting(name, event.target.dataset.option);
		});
		updateSetting(name, value);
	}
}



/* ==========
	BOOKS
========== */

/* -------
NAVIGATION
------- */

// change document title and menu-bar ToC
function navigateToSection() {
	if (window.location.hash.length == 0) {
		document.querySelector(`[aria-current="page"]`)?.removeAttribute(`aria-current`);
		page.contents.querySelector("a").setAttribute(`aria-current`, `page`);
		document.title = page.title.dataset.original;
		return;
	}

	// find article that is/has hash target
	const article = document.querySelector(window.location.hash)?.closest(`main > article`);

	// if the article exists, make its main nav-link the current page and update document title
	if (article) {
		const navLink = document.querySelector(`nav [href="#${article.id}"]`);
		document.querySelector(`[aria-current="page"]`)?.removeAttribute(`aria-current`);
		document.title = `${navLink.dataset.title ?? navLink.innerText} / ${page.title.dataset.original}`;
		navLink.setAttribute(`aria-current`, `page`);
	} else {
		window.location.hash = ``;
		navigateToSection();
	}
}

/* --------
FONT GUIDES
-------- */

/* GLYPH LISTS */

// convert character to unicode
function glyphToUnicode(glyph) {
	let unicode = glyph.charCodeAt().toString(16).toUpperCase();
	if (unicode.length < 4) unicode = `${`0`.repeat(4 - unicode.length)}${unicode}`;

	return unicode;
}

// build glyph list entry
function buildGlyphListEntry(glyph) {
	return `<div>
		<dt>${glyph}</dt>
		<dd><kbd>${glyph}</kbd></dd>
		${glyph.length === 1 ? `<dd><code>${glyphToUnicode(glyph)}</code></dd>` : ``}
	</div>`;
}

// build glyph lists
function buildGlyphLists() {
	if (typeof glyphsBySet === `undefined`) return;

	for (const [setName, glyphs] of Object.entries(glyphsBySet)) {
		document.querySelectorAll(`[data-glyph-set="${setName}"]`).forEach(list => {
			list.innerHTML = glyphs.split(` `).map(buildGlyphListEntry).join( ``);
		});
	}

	glyphsBySet = null;
}

/* FONT DEMOS */

// build style rule for applying a font feature
function buildFontFeatureStyle(feature) {
	return `.font-feature-demo-${feature.code} dt { font-feature-settings: "${feature.code}" 1; }`;
}

// build button to apply font feature
function buildFontFeatureButton(feature) {
	return `<li><button type="button" data-feature="${feature.code}" aria-pressed="false">${feature.name}</button></li>`;
}

// build out font features stylesheet and demo buttons
function buildFontFeatures() {
	if (typeof openTypeFeatures === `undefined`) return;

	document.head.appendChild(document.createElement(`style`)).innerHTML = openTypeFeatures.map(buildFontFeatureStyle).join(``);
	page.fontFeatureDemoButtons.innerHTML = openTypeFeatures.map(buildFontFeatureButton).join(``);

	// toggle a feature and the button for that feature on click
	page.fontFeatureDemoButtons.addEventListener(`click`, () => {
		if (event.target.tagName === `BUTTON` && !event.target.hasAttribute(`aria-disabled`)) {
			event.target.setAttribute(`aria-pressed`, event.target.getAttribute(`aria-pressed`) === `true` ? `false` : `true`);
			fontDemoFeatures.toggle(`"${event.target.dataset.feature}"`);
			page.customTextFontDemo.style.setProperty(`font-feature-settings`, [...fontDemoFeatures].join(`,`));
		}
	});

	openTypeFeatures = null;
}

// build button to apply font language
function buildFontLanguageButton(language) {
	return `<li><button type="button" data-lang="${language.code}" aria-pressed="false">${language.name}</button></li>`;
}

// build out font language demo buttons
function buildFontLanguages() {
	if (typeof openTypeLanguages === `undefined`) return;

	page.fontLanguageDemoButtons.innerHTML = openTypeLanguages.map(buildFontLanguageButton).join(``);

	pressButton(page.fontLanguageDemoButtons.querySelector(`button`));
	page.customTextFontDemo.setAttribute(`lang`, openTypeLanguages[0].code);

	// toggle a language and the button for that language on click (untoggle previous language)
	page.fontLanguageDemoButtons.addEventListener(`click`, () => {
		if (event.target.tagName === `BUTTON` && !event.target.hasAttribute(`aria-disabled`)) {
			unpressButton(page.fontLanguageDemoButtons.querySelector(`[aria-pressed="true"]`));
			pressButton(event.target);

			page.customTextFontDemo.setAttribute(`lang`, event.target.dataset.lang);
		}
	});

	openTypeLanguages = null;
}



/* ===========
	EVENTS
=========== */

// navigate to section on hashchange
window.addEventListener(`hashchange`, () => navigateToSection());

// change displayed settings on pageload and add event listeners on button sets
document.addEventListener(`DOMContentLoaded`, () => {
	/* INDEX */
	buildSettingsButtons();

	/* FONT GUIDES */
	buildGlyphLists();
	buildFontFeatures();
	buildFontLanguages();

	// update page head data
	page.title.dataset.original = document.title;
	if (window.location.hash) navigateToSection();
});

// on closing page, store settings in local storage
window.addEventListener(`beforeunload`, () => window.localStorage.setItem(`settings`, JSON.stringify(settings)));
