<!DOCTYPE html>
<html lang="en-GB" data-theme="light" data-italics="normal" data-lines="normal">
<head>
	<meta charset="utf-8">
	<title>SRB2K server setup walkthrough</title>

	<link rel="preload" type="font/woff2" href="../fonts/Comfortaa.woff2?v=2023-10-18" as="font" crossorigin>

	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../main.css?v=2024-07-14" media="all">

	<link rel="icon" href="../images/favicon.svg?v=2023-10-29" type="image/svg+xml" sizes="any">
	<link rel="icon" href="../images/favicon.ico?v=2023-11-15" sizes="48x48">

	<meta name="og:image" 		content="https://lanternlibrary.com/images/social-card.png">
	<meta name="twitter:card" 	content="summary">

	<link rel="alternate" type="application/rss+xml" href="https://lanternlibrary.com/feed.rss" title="RSS feed for the Lantern Library">

	<script src="../initialisation.js?v=2024-06-30"></script>
</head>



<body>
<div id="container">
<header>
	<a href="#main-content" id="skip-link">Skip to main content</a>
	<h1>
		<img src="../assets/srb2k-server-setup-walkthrough/images/header.png" alt="SRB2K's terminal startup text, including: We hope you enjoy this game as much as we did making it!">
		<span><cite><abbr title="Sonic Robo Blast 2 Kart">SRB2K</abbr></cite> server setup walkthrough</span>
	</h1>
</header>



<nav id="sidebar">
	<a href="../index.html" rel="index">Index</a>
	<ol id="contents">
		<li>
			<a href="#home" aria-current="page">Home</a>
			<ol>
				<li><a href="#home-requirements">Requirements</a></li>
			</ol>
		</li>
		<li>
			<a href="#preparation">Preparation</a>
			<ol>
				<li><a href="#preparation-organising-your-files">Organising your files</a></li>
				<li><a href="#preparation-making-an-account">Making an account</a></li>
			</ol>
		</li>
		<li>
			<a href="#walkthrough">Walkthrough</a>
			<ol>
				<li><a href="#walkthrough-server-space">Server space</a></li>
				<li><a href="#walkthrough-groundwork">Groundwork</a></li>
				<li><a href="#walkthrough-transferring-files">Transferring files</a></li>
				<li><a href="#walkthrough-getting-the-game">Getting the game</a></li>
				<li><a href="#walkthrough-automation">Automation</a></li>
				<li><a href="#walkthrough-final-steps">Final steps</a></li>
			</ol>
		</li>
		<li><a href="#restarting-the-game">Restarting the Game</a></li>
		<li><a href="#persistent-data">Persistent Data</a></li>
		<li>
			<a href="#config">Config</a>
			<ol>
				<li><a href="#config-examples">Examples</a></li>
			</ol>
		</li>
		<li>
			<a href="#addons">Addons</a>
			<ol>
				<li><a href="#addons-examples">Examples</a></li>
			</ol>
		</li>
		<li><a href="#advice">Advice</a></li>
		<li><a href="#cliff-notes">Cliff Notes</a></li>
	</ol>
</nav>



<main id="main-content">
<article id="preparation">
	<h2>Preparation</h2>
	<p>There's a few steps to take before getting underway. Generally speaking, once you've done these, you don't need to do them again from scratch.</p>



	<h3 id="preparation-organising-your-files">Organising your files</h3>
	<p>Before you start setting up the server you'll need to organise the files you're going to transfer&mdash;your config, addons, and a few other things.</p>
	<ol>
		<li>download <a href="../assets/srb2k-server-setup-walkthrough/files/server.zip">this mostly-empty server template</a></li>
		<li>unzip, then delete <code>server.zip</code></li>
		<li>enter the <code>server</code> folder</li>
		<li>sort your <a href="#addons">addons</a> into the subfolders in the addons folder (<code>characters</code>, <code>maps</code>, and <code>other</code>); don't put any addons in the main addons folder, and leave the <code>dud.txt</code> files where they are</li>
		<li>write your <a href="#config">server config</a> in the <code>kartserv.cfg</code> file in the <code>config</code> folder</li>
		<li>re-compress the <code>server</code> folder to create <code>server.zip</code></li>
	</ol>
	<p>Later on you're going to transfer your copy of <code>server.zip</code> to your server using your <abbr>FTP</abbr> app.</p>



	<h3 id="preparation-making-an-account">Making an account</h3>
	<p>The easiest way to run a freestanding kart server is (probably) on a cheap commercial virtual private server (<abbr>VPS</abbr>). There's a few providers around that let you quickly and cheaply set up a <abbr>VPS</abbr>, such as <a href="https://digitalocean.com/" rel="external">DigitalOcean</a>, <a href="http://linode.com/" rel="external">Linode</a>, and <a href="https://vultr.com/" rel="external">Vultr</a>. Generally the service you need costs about $5 per month from any provider if you're running it constantly.</p>
	<p>Before starting setup, you need to register an account with a hosting provider. If it helps resolve choice paralysis, I use DigitalOcean and haven't had any major problems with their service. A few bits of wording in this walkthrough are written with DigitalOcean in mind, but it should apply anywhere.</p>
</article>



<article id="walkthrough">
	<h2>Walkthrough</h2>
	<p>Follow these steps to set up a server. For a shorter version without explanations, check the <a href="#cliff-notes">Cliff Notes</a>.</p>



	<h3 id="walkthrough-server-space">Server space</h3>
	<p>First, set up your <abbr>VPS</abbr> by entering your default project and creating a new &ldquo;droplet&rdquo; (or whatever your provider calls their <abbr>VPS</abbr>es if they don't just call them <abbr>VPS</abbr>es). For <strong>location</strong>, choose a data center closest to your players. For <strong>image</strong>, you can probably choose the default. For <strong>plan</strong>, choose the cheapest that has at least these specs:</p>
	<dl class="keys-values">
		<dt>core/<abbr title="Central Processing Unit">CPU</abbr></dt>
		<dd>1</dd>
		<dt><abbr title="Random-Access Memory">RAM</abbr></d>
		<dd>1<abbr title="gigabyte">GB</abbr></dd>
		<dt><abbr title="Solid State Drive">SSD</abbr> storage</dt>
		<dd>25<abbr>GB</abbr></dd>
		<dt>transfer</dt>
		<dd>1<abbr title="terabyte">TB</abbr> per month upload/download speed</dd>
		<dt><abbr>IP</abbr> address</dt>
		<dd><em>must</em> have <abbr>IPv4</abbr> (<abbr>IPv6</abbr> doesn't matter)</dd> 
	</dl>
	<p>Finally, for <strong>security</strong>, it's easier to create a password, but more secure to make an <abbr title="Secure Shell">SSH</abbr> key; go with whatever you prefer</p>
	<p>For my server I use a DigitalOcean droplet in New York running Ubuntu 23.10 x64 on the Basic plan (shared <abbr>CPU</abbr>) with the specs above. This costs $6 per month of uptime right now (late 2023), but the real cost's much lower because I destroy the droplet after playing and rebuild it for the next session, so it's only active a few hours a week at most and the cost per hour's currently less than ¢1.</p>
	<p class="call-out info">The specs above are more than you need, but better safe than sorry. You can go cheaper as long as you still get the <abbr>IPv4</abbr> address.</p>



	<h3 id="walkthrough-groundwork">Groundwork</h3>
	<p>Now you're going to create a new user in the <abbr>VPS</abbr> and give them some extra permissions. This user's going to be where you actually install the game and transfer <a href="#preparation-organising-your-files">your files</a>. This helps keep this stuff separate from the <code>root</code> user's powerful capabilities.</p>
	<ol>
		<li>access the <abbr>VPS</abbr> using its console (on DigitalOcean: click the project name, then the droplet name, then &ldquo;Access&rdquo;; it may take a minute or two to become fully functional if you've just created the droplet)</li>
		<li>open the console as <code>root</code> (the admin user)</li>
		<li>run <code>adduser srb2kart</code> and follow the instructions (the only thing that matters is making a password, though, just leave the rest empty)</li>
		<li>run <code>usermod -aG sudo srb2kart</code></li>
		<li>close the console</li>
	</ol>
	<p>From now on, any time you launch the console, launch it as <strong><code>srb2kart</code></strong>. Note that the console will still sometimes ask you to enter their password to run certain commands (mostly just the first <code>sudo</code> command you run in each console session).</p>



	<h3 id="walkthrough-transferring-files">Transferring files</h3>
	<p>Access your <abbr>VPS</abbr> using your <abbr>FTP</abbr> app with these login details:</p>
	<dl class="keys-values">
		<dt>Host</dt>
		<dd>your <abbr>VPS</abbr>' <abbr>IPv4</abbr> address, listed on its page</dd>
		<dt>Username</dt>
		<dd>srb2kart</dd>
		<dt>Password</dt>
		<dd>the password you set for the srb2kart user</dd>
		<dt>Port</dt>
		<dd>22</dd>
	</dl>
	<p>You can now drag and drop your <code>server.zip</code> from your computer to the <code>srb2kart</code> folder on your <abbr>VPS</abbr>. Depending on how large your addons folder is, this may take several minutes, but you can get on with the next steps while it's transferring.</p>



	<h3 id="walkthrough-getting-the-game">Getting the game</h3>
	<p>Now you can set up the game itself. You're going to use a docker&mdash;a portable container with the game inside. The <a href="https://github.com/ellitedev/srb2kart-server-docker" rel="external">most up-to-date docker</a> is provided by ellitedev and works for <cite><abbr>SRB2K</abbr></cite> v1.6 (the current and final version of the game). You don't need to download anything from the source&mdash;the commands in this section do everything for you.</p>
	<ol>
		<li>open the console as <code>srb2kart</code> (the new user)</li>
		<li>run <code>sudo snap install docker</code> and wait for it to finish</li>
		<li>run <code>sudo docker run -it --name srb2kart -p 5029:5029/udp ellite/srb2kart-server:latest</code></li>
		<li>wait until the game starts idling: <samp>No nodes at round start, idling...</samp></li>
		<li>close the console</li>
	</ol>
	<p>These commands set up the game, but in a very limited way&mdash;no addons, no config, and the server shuts down when you close the console. The rest of this walkthrough explains how to fix all that.</p>



	<h3 id="walkthrough-automation">Automation</h2>
	<p>First, follow these instructions to let your kart server start running when someone connects even if the console isn't open:</p>
	<ol>
		<li>open the console as <code>srb2kart</code></li>
		<li>run <code>cd /etc/systemd/system</code></li>
		<li>run <code>sudo nano docker.srb2kart.service</code></li>
		<li>
			<span>copy and paste this text into the newly-created file:</span>
			<code class="code-block">[Unit]
Description=SRB2Kart Server
Requires=docker.service
After=docker.service
# Ref: https://www.freedesktop.org/software/systemd/man/systemd.unit.html#StartLimitIntervalSec=interval
StartLimitIntervalSec=60s
StartLimitBurst=2

[Service]
TimeoutStartSec=0
Restart=on-failure
RestartSec=5s
ExecStartPre=/usr/bin/docker stop %n
ExecStartPre=/usr/bin/docker rm %n
ExecStartPre=/usr/bin/docker pull ellite/srb2kart-server:<version>
ExecStart=/usr/bin/docker run --rm --name %n \
	-v /home/srb2kart/server/config:/config \
	-v /home/srb2kart/server/addons:/addons \
	-v /home/srb2kart/server/luafiles:/luafiles \
	-p 5029:5029/udp \
	ellite/srb2kart-server:1.6

[Install]
WantedBy=multi-user.target</code>
		</li>
		<li>save the file using <kbd><kbd>Ctrl</kbd> + <kbd>O</kbd></kbd>, then <kbd>Enter</kbd></li>
		<li>exit the file using <kbd><kbd>Ctrl</kbd> + <kbd>X</kbd></kbd></li>
		<li>run <code>systemctl enable docker.srb2kart</code></li>
	</ol>
	<p>This creates and enables a &ldquo;service&rdquo;&mdash;in this case a set of commands that starts up the kart server and connects it to your addons and config. Your <abbr>VPS</abbr> runs these commands when someone connects to its <abbr>IP</abbr> in the game's multiplayer menu (unless the server's already running, in which case new players just join the existing game).</p>



	<h3 id="walkthrough-final-steps">Final steps</h3>
	<p>Wait until <code>server.zip</code> finishes transferring before continuing. Finally, you can add your config and addons:</p>
	<ol>
		<li>run <code>cd</code></li>
		<li>run <code>sudo docker rm -f srb2kart</code></li>
		<li>run <code>sudo apt-get install unzip</code> and wait for it to finish</li>
		<li>run <code>unzip server.zip</code></li>
		<li>run <code>bash server/scripts/update_addons.sh</code></li>
		<li>close the console</li>
		<li>shut down the <abbr>VPS</abbr> (on DigitalOcean: click the &ldquo;On/Off&rdquo; toggle)</li>
		<li>start up the <abbr>VPS</abbr></li>
		<li>open the console as <code>srb2kart</code> (when it starts working again)</li>
		<li>
			<span>run the following:</span>
			<code class="code-block">sudo docker run -it --name srb2kart -p 5029:5029/udp \
-v /home/srb2kart/server/config:/config \
-v /home/srb2kart/server/addons:/addons \
-v /home/srb2kart/server/luafiles:/luafiles \
ellite/srb2kart-server:latest</code>
		</li>
		<li>wait until the game starts idling (it may also prompt you to hit <kbd>Enter</kbd> or take other actions while it starts up, before it can begin idling)</li>
		<li>close the console</li>
	</ol>
	<p>At this point, your server's fully up and running: it's using your config, installing your addons, and can run when anyone connects even if the console isn't open and running the game. Once you know this whole process it should only take about 10&ndash;20 minutes to get from creating a <abbr>VPS</abbr> to running the full game, addons and all.</p>
	<p class="call-out info"><code>update_addons.sh</code> is a script that deletes any files in the main addons folder, then copies all files in the <code>characters</code>, <code>maps</code>, and <code>other</code> subfolders (including subfolders of those three folders) into the main addons folder. The script fails if any of the three folders have no files inside. This is why the <code>server.zip</code> template has empty text files called <code>dud.txt</code> in each of those folders&mdash;so there's always something to copy, even if it's basically nothing. You need to put all your addons in one folder because the game won't find addons in subfolders of the addons folder, but putting all your addons in one folder can make things hard to find.</p>
</article><!--#walkthrough end-->



<article id="restarting-the-game">
	<h2>Restarting the Game</h2>
	<p>If you edit, add, or remove addon or config files on the server, or need to restart the game for any other reason, then follow these steps:</p>
	<ol>
		<li>open the console as <code>srb2kart</code></li>
		<li>run <code>sudo docker rm -f srb2kart</code></li>
		<li>
			<span>run the following:</span>
			<code class="code-block">sudo docker run -it --name srb2kart -p 5029:5029/udp \
-v /home/srb2kart/server/config:/config \
-v /home/srb2kart/server/addons:/addons \
-v /home/srb2kart/server/luafiles:/luafiles \
ellite/srb2kart-server:latest</code>
		</li>
	</ol>
</article><!--#restarting-the-game end-->



<article id="persistent-data">
	<h2>Persistent Data</h2>
	<p>What about the fourth folder in the <code>server</code> folder, <code>data</code>? This lets the game store its state when everyone leaves the server and restart from the same state when people rejoin. In my opinion, it only matters if you're being <em>really</em> serious about points, but here's how to bind it to the docker if you care about that stuff.</p>
	<p>Any time this walkthrough says to run the full docker command, run this version with with the marked extra line:</p>
	<code class="code-block">sudo docker run -it --name srb2kart -p 5029:5029/udp \
-v /home/srb2kart/server/config:/config \
-v /home/srb2kart/server/addons:/addons \
-v /home/srb2kart/server/luafiles:/luafiles \
<mark>-v /home/srb2kart/server/data:/data \
</mark>ellite/srb2kart-server:latest</code>
	<p>You'll also need to update the matching commands in the <code>docker.srb2kart.service</code> file you created to <a href="#walkthrough-automation">automate the server</a>.</p>
</article><!--#persistent-data end-->



<article id="config">
	<h2>Config</h2>
	<p>To change your server's configuration (e.g. change how the game works):</p>
	<ol>
		<li>edit the <code>kartserv.cfg</code> file on your computer</li>
		<li>use your <abbr>FTP</abbr> app to transfer the file to the <code>config</code> folder on your <abbr>VPS</abbr> (overwriting the existing one)</li>
		<li><a href="#restarting-the-game">restart the docker</a></li>
	</ol>
	<p>A config file consists of a list of commands, one per line. The file can include blank or commented lines. Each command consists of a name and, normally, one or more &ldquo;arguments&rdquo; (inputs). For instance, a lot of them let you switch a setting on or off, and are written like this: <code>[command name] [on/off]</code>. There are <a href="https://wiki.srb2.org/wiki/User:ThatAwesomeGuy173/SRB2Kart/Commands" rel="external">many</a>, <a href="https://wiki.srb2.org/wiki/User:ThatAwesomeGuy173/SRB2Kart/Variables" rel="external">many</a> commands you can use to mechanically tweak the game.</p>
	<p>Here's an example config file:</p>
	<code class="code-block">wait 10
password hunter2

// here's a comment in its own line;
// the game ignores all text between the double-slash and the end of the line

kartspeed 2 // here's a comment after a command, on the same line
// karteliminatelast off // here's a command that's deactivated by commenting it out</code>
	<p class="call-out warning">Some config commands <em>must</em> run a shortly <em>after</em> the game starts, otherwise they won't take effect. Make sure there's the command <code>wait 10</code> at the start of the file so the game's already running before it applies any of your config.</p>



	<h3 id="config-examples">Examples</h3>
	<p>Here are some of the more useful commands available in the game itself (no mods needed) if you're just starting out:</p>
	<dl class="keys-values">
		<dt><code>showjoinaddress off</code></dt>
		<dd>hides players' <abbr>IP</abbr> addresses in the <abbr>VPS</abbr>' console</dd>
		<dt><code>password [password]</code></dt>
		<dd>sets a password you can enter in the in-game console to gain admin status, e.g. <code>password hunter2</code></dd>
		<dt><code>login [password]</code></dt>
		<dd>use this in the in-game console (accessed by pressing <kbd>`</kbd> (or <kbd>§</kbd> on mac) to gain admin status, e.g. <code>login hunter2</code></dd>
		<dt><code>kartspeed 0/1/2</code></dt>
		<dd>sets the game speed to 0/easy (battle speed), 1/normal (the default in multiplayer races), or 2/hard (record attack speed)</dd>
		<dt><code>karteliminatelast on/off</code></dt>
		<dd>sets whether players start to explode once most players have finished the race (defaults to on)</dd>
		<dt><code>kartfrantic on/off</code></dt>
		<dd>sets whether powerful items appear more or less often (defaults to off)</dd>
		<dt><code>advancemap 0/1/2/3</code></dt>
		<dd>sets how the next map's decided after each round: 0/the same, 1/next in rotation, 2/random, 3/vote (defaults to 3)</dd>
	</dl>
	<p>Many non-map/character <a href="#addons">addons</a> can be controlled using config commands; some (e.g. Hostmod) are completely inactive until activated this way (in config files or the in-game console).</p>
</article><!--#config end-->



<article id="addons">
	<h2>Addons</h2>
	<p>To add addons/mods to your server (e.g. map packs, extra characters, new features):</p>
	<ol>
		<li>use your <abbr>FTP</abbr> app to transfer the files to the right subfolder in the <code>addons</code> folder on your <abbr>VPS</abbr> (or overwrite or delete existing files)</li>
		<li>open the console as <code>srb2kart</code></li>
		<li>run <code>bash server/scripts/update_addons.sh</code></li>
		<li><a href="#restarting-the-game">restart the docker</a></li>
	</ol>
	<p>You should always make the same changes to the files and folders on your own computer, e.g. if you add a new character file to <code>server/addons/characters</code> on the <abbr>VPS</abbr> then you should do the same thing on your computer. This way, if you need to rebuild the server from scratch, you can just re-zip the <code>server</code> folder on your computer and transfer it to the new <abbr>VPS</abbr>.</p>



	<h3 id="addons-examples">Examples</h3>
	<p>Here are some addons you may find useful in multiplayer:</p>
	<dl class="keys-values">
		<dt><code>bonuschars.kart</code></dt>
		<dd>the pack of bonus characters that comes with the game; useful if you don't already have extra characters</dd>
		<dt><a href="https://mb.srb2.org/addons/hostmod.2421/" rel="external">Hostmod</a></dt>
		<dd>a broad and powerful set of quality-of-life improvements (see <a href="https://hyuu.cc/hostmod/" rel="external">documentation</a> for more details)</dd>
		<dt><a href="https://mb.srb2.org/addons/hitfeed-kl_hitfeed_v2-2-pk3.2399/" rel="external">Hitfeed</a></dt>
		<dd>a feed of attacks, displayed at the top of the screen</dd>
		<dt><a href="https://mb.srb2.org/addons/driftboost-gauge.2341/" rel="external">Drift Gauge</a></dt>
		<dd>a meter that shows how close you are to charging turbos while drifting</dd>
	</dl>
	<p>Some addons (e.g. all Hostmod features) need to be activated using commands. Some of these make sense to write in your <a href="#config">config</a> file so they'll automatically be applied when the server starts; others make sense to use in the in-game console as and when you need them. You'll have to decide this case by case.</p>
</article><!--#addons end-->



<article id="advice">
	<h2>Advice</h2>
	<p>Here's a few things to keep in mind:</p>
	<ul>
		<li>
			<p>It'll always take a little time for the <abbr>VPS</abbr> to fully start up for the first time or after being shut down. If you can't connect in the console, close it and reopen it after a minute.</p>
		</li>
		<li>
			<p>The kart server will probably crash after idling for a long time, possibly due to a memory leak in the game. If this happens, <a href="#restarting-the-game">restart the game</a>. You may also need to restart the <abbr>VPS</abbr> if you can't connect or your framerate's grindingly slow.</p>
		</li>
		<li>
			<p>On services like DigitalOcean you should only be charged for the time your <abbr>VPS</abbr> exists (or that you're storing a backup of it). If you only play a few hours a week (e.g. 3, 5, 10) it'll be much cheaper to destroy your <abbr>VPS</abbr> when you're done playing and rebuild it when you next want to play. It only takes me 10 minutes  to follow the <a href="#walkthrough">Walkthrough</a>, including letting files transfer in the background; it might take you longer at first, but it's simple once you get used to it.</p>
		</li>
		<li>
			<p>Your hosting provider may not install certain apps on their <abbr>VPS</abbr>es by default, e.g. they may not have <code>snap</code> (<a href="#walkthrough-getting-the-game">used to install <code>docker</code></a>) or <code>apt-get</code> (<a href="#walkthrough-final-steps">used to install <code>unzip</code></a>). In these cases the console should provide alternative instructions for installing software.</p>
		</li>
	</ul>
</article><!--#advice end-->



<article id="cliff-notes">
	<h2>Cliff Notes</h2>
	<p>This is a list of all instructions in the <a href="#walkthrough">Walkthrough</a> with no extra info or explanations&mdash;it's a collection of all the pieces of code (and other instructions) in one place.</p>
	<ol>
		<li>set up your <abbr>VPS</abbr></li>
		<li>open the console as <code>root</code></li>
		<li>run <code>adduser srb2kart</code> and follow the instructions</li>
		<li>run <code>usermod -aG sudo srb2kart</code></li>
		<li>close the console</li>
		<li>transfer <code>server.zip</code> to the <code>srb2kart</code> folder of the <code>srb2kart</code> user</li>
		<li>open the console as <code>srb2kart</code></li>
		<li>run <code>sudo snap install docker</code> and wait for it to finish</li>
		<li>run <code>sudo docker run -it --name srb2kart -p 5029:5029/udp ellite/srb2kart-server:latest</code> and wait for the game to start idling</li>
		<li>close the console and re-open it as <code>srb2kart</code></li>
		<li>run <code>cd /etc/systemd/system</code></li>
		<li>run <code>sudo nano docker.srb2kart.service</code></li>
		<li>
			<span>copy and paste this text into the file, then save and exit:</span>
			<code class="code-block">[Unit]
Description=SRB2Kart Server
Requires=docker.service
After=docker.service
# Ref: https://www.freedesktop.org/software/systemd/man/systemd.unit.html#StartLimitIntervalSec=interval
StartLimitIntervalSec=60s
StartLimitBurst=2

[Service]
TimeoutStartSec=0
Restart=on-failure
RestartSec=5s
ExecStartPre=/usr/bin/docker stop %n
ExecStartPre=/usr/bin/docker rm %n
ExecStartPre=/usr/bin/docker pull ellite/srb2kart-server:<version>
ExecStart=/usr/bin/docker run --rm --name %n \
	-v /home/srb2kart/server/config:/config \
	-v /home/srb2kart/server/addons:/addons \
	-v /home/srb2kart/server/luafiles:/luafiles \
	-p 5029:5029/udp \
	ellite/srb2kart-server:1.6

[Install]
WantedBy=multi-user.target</code>
		</li>
		<li>run <code>systemctl enable docker.srb2kart</code></li>
		<li>run <code>cd</code></li>
		<li>run <code>sudo docker rm -f srb2kart</code></li>
		<li>run <code>sudo apt-get install unzip</code> and wait for it to finish</li>
		<li>run <code>unzip server.zip</code></li>
		<li>run <code>bash server/scripts/update_addons.sh</code></li>
		<li>close the console</li>
		<li>shut down and restart the <abbr>VPS</abbr></li>
		<li>open the console as <code>srb2kart</code></li>
		<li>
			<span>run the following and wait for the game to start idling:</span>
			<code class="code-block">sudo docker run -it --name srb2kart -p 5029:5029/udp \
-v /home/srb2kart/server/config:/config \
-v /home/srb2kart/server/addons:/addons \
-v /home/srb2kart/server/luafiles:/luafiles \
ellite/srb2kart-server:latest</code>
		</li>
		<li>close the console</li>
	</ol>
</article><!--#cliff-notes end-->



<article id="home">
	<h2>Home</h2>
	<p>This is a walkthrough for setting up and managing an online multiplayer server for <cite>Sonic Robo Blast 2 Kart</cite> (<cite><abbr>SRB2K</abbr></cite>) so you can play online with privacy and stability for everyone.</p>
	<p>The main sections are <a href="#preparation">Preparation</a> and <a href="#walkthrough">Walkthrough</a>. Everything else is advice and info on maintaining your server once it's up and running.</p>
	<p>This <em>will</em> get a little technical, but it's not that complicated. Mostly you just need to copy and paste bits of code and hit enter.</p>
	<p class="call-out warning">This walkthrough's only for making a private server. If you want your server to show up in the game's server browser then you'll need to do extra work and maintenance beyond the scope of this walkthrough.</p>



	<h3 id="home-requirements">Requirements</h3>
	<p>You will need:</p>
	<ul>
		<li><a href="https://mb.srb2.org/addons/srb2kart.2435/" rel="external">the game</a> on your computer</li>
		<li>a few dollars per month (at most) to pay for hosting</li>
		<li>an <abbr title="File Transfer Protocol">FTP</abbr> client app (e.g. <a href="https://filezilla-project.org" rel="external">FileZilla</a>)</li>
	</ul>
	<p>Since the game's available for all common operating systems, and since most of this process takes place on a server somewhere, it doesn't really matter what operating system you personally use.</p>
</article><!--#home end-->
</main>
</div><!--#container end-->



<script src="../main.js?v=2024-07-14"></script>
</body>
</html>
