<!DOCTYPE html>
<html lang="en-GB" data-theme="light" data-italics="normal" data-lines="normal">
<head>
	<meta charset="utf-8">
	<title>Custom picoCAD shapes guide</title>

	<link rel="preload" type="font/woff2" href="../fonts/Comfortaa.woff2?v=2023-10-18" as="font" crossorigin>

	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../main.css?v=2024-07-14" media="all">

	<link rel="icon" href="../images/favicon.svg?v=2023-10-29" type="image/svg+xml" sizes="any">
	<link rel="icon" href="../images/favicon.ico?v=2023-11-15" sizes="48x48">

	<meta name="og:image" 		content="https://lanternlibrary.com/images/social-card.png">
	<meta name="twitter:card" 	content="summary">

	<link rel="alternate" type="application/rss+xml" href="https://lanternlibrary.com/feed.rss" title="RSS feed for the Lantern Library">

	<script src="../initialisation.js?v=2024-06-30"></script>
</head>



<body>
<div id="container">
<header>
	<a href="#main-content" id="skip-link">Skip to main content</a>
	<h1>
		<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/header.png" alt="">
		<span>Custom pico<abbr title="Computer-Aided Design">CAD</abbr> shapes guide</span>
	</h1>
</header>



<nav id="sidebar">
	<a href="../index.html" rel="index">Index</a>
	<ol id="contents">
		<li>
			<a href="#home" aria-current="page">Home</a>
			<ol>
				<li><a href="#home-using-these-shapes">Using These Shapes</a></li>
				<li><a href="#home-errors">Errors</a></li>
			</ol>
		</li>
		<li>
			<a href="#platonic-solids">Platonic Solids</a>
			<ol>
				<li><a href="#platonic-solids-tetrahedron">Tetrahedron</a></li>
				<li><a href="#platonic-solids-octahedron">Octahedron</a></li>
				<li><a href="#platonic-solids-dodecahedron">Dodecahedron</a></li>
				<li><a href="#platonic-solids-icosahedron">Icosahedron</a></li>
			</ol>
		</li>
		<li>
			<a href="#pyramids">Pyramids</a>
			<ol>
				<li><a href="#pyramids-pentagonal">Pentagonal</a></li>
				<li><a href="#pyramids-hexagonal">Hexagonal</a></li>
				<li><a href="#pyramids-octagonal">Octagonal</a></li>
			</ol>
		</li>
		<li>
			<a href="#antiprisms">Antiprisms</a>
			<ol>
				<li><a href="#antiprisms-triangular">Triangular</a></li>
				<li><a href="#antiprisms-square">Square</a></li>
				<li><a href="#antiprisms-pentagonal">Pentagonal</a></li>
				<li><a href="#antiprisms-hexagonal">Hexagonal</a></li>
				<li><a href="#antiprisms-octagonal">Octagonal</a></li>
			</ol>
		</li>
	</ol>
</nav>



<main id="main-content">
<article id="platonic-solids">
	<h2>Platonic Solids</h2>
	<p>Platonic solids are five 3-dimensional shapes whose faces are also regular 2-dimensional shapes; all edges of a Platonic solid are the same length and all of its internal angles are the same size. The five Platonic solids are:</p>
	<ul>
		<li>the <strong>tetrahedron</strong></li>
		<li>the <strong>cube</strong> (already in pico<abbr>CAD</abbr>)</li>
		<li>the <strong>octahedron</strong></li>
		<li>the <strong>dodecahedron</strong></li>
		<li>the <strong>icosahedron</strong></li>
	</ul>
	<p>Most of the Platonic solids included in this file are provided in two forms: on-grid and off-grid. The coordinates for on-grid forms are in steps of 0.25; the coordinates for off-grid forms are much more precise. However, the differences are still pretty small.</p>



	<h3 id="platonic-solids-tetrahedron">Tetrahedron</h3>
	<p>The regular tetrahedron has 4 vertices and 4 faces, each an equilateral triangle.</p>
	<p>This is the data for an on-grid tetrahedron:</p>
	<code class="code-block">{
 name='tetrahedron', pos={0,0,0}, rot={0,0,0},
 v={
  {1,0.5,0},
  {-0.5,0.5,0.75},
  {-0.5,0.5,-0.75},
  {0,-0.75,0}
 },
 f={
  {3,1,2, c=11, uv={3,0,2,1.25,1,0} },
  {4,2,1, c=11, uv={8,1.25,9,0,7,0} },
  {4,1,3, c=11, uv={6,1.25,5,0,7,0} },
  {2,4,3, c=11, uv={5,0,4,1.25,3,0} }
 } 
}</code>
	<p>This is the data for an off-grid, i.e. more-perfect, tetrahedron:</p>
	<code class="code-block">{
 name='tetrahedron', pos={0,0,0}, rot={0,0,0},
 v={
  {0.943,0.5,0},
  {-0.471,0.5,0.816},
  {-0.471,0.5,-0.816},
  {0,-0.8329,0}
 },
 f={
  {3,1,2, c=11, uv={3,0,2,1.25,1,0} },
  {4,2,1, c=11, uv={8,1.25,9,0,7,0} },
  {4,1,3, c=11, uv={6,1.25,5,0,7,0} },
  {2,4,3, c=11, uv={5,0,4,1.25,3,0} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<figure>
				<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/tetrahedron-on-grid.gif" alt="">
				<figcaption>On-grid</figcaption>
			</figure>
		</li>
		<li>
			<figure>
				<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/tetrahedron-off-grid.gif" alt="">
				<figcaption>Off-grid</figcaption>
			</figure>
		</li>
	</ul>



	<h3 id="platonic-solids-octahedron">Octahedron</h3>
	<p>The regular octahedron has 6 vertices and 8 faces, each an equilateral triangle.</p>
	<p>Octahedral coordinates are all on-grid, so there's only one form:</p>
	<code class="code-block">{
 name='octahedron', pos={0,0,0}, rot={0,0,0},
 v={
  {1,0,0},
  {-1,0,0},
  {0,1,0},
  {0,-1,0},
  {0,0,1},
  {0,0,-1}
 },
 f={
  {4,5,1, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,1,6, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,6,2, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,2,5, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {3,1,5, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {3,5,2, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {3,2,6, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {3,6,1, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<figure>
				<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/octahedron.gif" alt="">
				<figcaption>On-grid</figcaption>
			</figure>
		</li>
	</ul>
	<p class="call-out info">A <a href="#antiprisms-triangular">triangular antiprism</a> is technically also an octahedron, but the two shapes in this guide are oriented differently.</p>



	<h3 id="platonic-solids-dodecahedron">Dodecahedron</h3>
	<p>The regular dodecahedron has 20 vertices and 12 faces, each a regular pentagon.</p>
	<p>This is the data for an on-grid dodecahedron:</p>
	<code class="code-block">{
 name='dodecahedron', pos={0,0,0}, rot={0,0,0},
 v={
  {-1,-1,1},
  {1,-1,1},
  {-1,1,1},
  {-1,-1,-1},
  {1,1,1},
  {1,-1,-1},
  {-1,1,-1},
  {1,1,-1},
  {-1.5,-0.5,0},
  {1.5,-0.5,0},
  {-1.5,0.5,0},
  {1.5,0.5,0},
  {0,-1.5,0.5},
  {0,-1.5,-0.5},
  {0,1.5,0.5},
  {0,1.5,-0.5},
  {-0.5,0,1.5},
  {-0.5,0,-1.5},
  {0.5,0,1.5},
  {0.5,0,-1.5}
 },
 f={
  {2,19,5,12,10, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {6,10,12,8,20, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {10,6,14,13,2, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {1,17,19,2,13, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {3,15,5,19,17, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {5,15,16,8,12, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {15,3,11,7,16, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {16,7,18,20,8, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {18,4,14,6,20, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {9,1,13,14,4, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {1,9,11,3,17, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {9,4,18,7,11, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} }
 } 
}</code>
	<p>This is the data for an off-grid, i.e. more-perfect, dodecahedron:</p>
	<code class="code-block">{
 name='dodecahedron', pos={0,0,0}, rot={0,0,0},
 v={
  {-1,-1,1},
  {1,-1,1},
  {-1,1,1},
  {-1,-1,-1},
  {1,1,1},
  {1,-1,-1},
  {-1,1,-1},
  {1,1,-1},
  {-1.618,-0.618,0},
  {1.618,-0.618,0},
  {-1.618,0.618,0},
  {1.618,0.618,0},
  {0,-1.618,0.618},
  {0,-1.618,-0.618},
  {0,1.618,0.618},
  {0,1.618,-0.618},
  {-0.618,0,1.618},
  {-0.618,0,-1.618},
  {0.618,0,1.618},
  {0.618,0,-1.618}
 },
 f={
  {2,19,5,12,10, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {6,10,12,8,20, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {10,6,14,13,2, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {1,17,19,2,13, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {3,15,5,19,17, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {5,15,16,8,12, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {15,3,11,7,16, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {16,7,18,20,8, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {18,4,14,6,20, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {9,1,13,14,4, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {1,9,11,3,17, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {9,4,18,7,11, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<figure>
				<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/dodecahedron-on-grid.gif" alt="">
				<figcaption>On-grid</figcaption>
			</figure>
		</li>
		<li>
			<figure>
				<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/dodecahedron-off-grid.gif" alt="">
				<figcaption>Off-grid</figcaption>
			</figure>
		</li>
	</ul>



	<h3 id="platonic-solids-icosahedron">Icosahedron</h3>
	<p>The regular icosahedron has 12 vertices and 20 faces, each an equilateral triangle.</p>
	<p>This is the data for an on-grid icosahedron:</p>
	<code class="code-block">{
 name='icosahedron', pos={0,0,0}, rot={0,0,0},
 v={
  {0,1,1.5},
  {0,-1,1.5},
  {0,1,-1.5},
  {0,-1,-1.5},
  {1,1.5,0},
  {-1,1.5,0},
  {1,-1.5,0},
  {-1,-1.5,0},
  {1.5,0,1},
  {-1.5,0,1},
  {1.5,0,-1},
  {-1.5,0,-1}
 },
 f={
  {1,6,5, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {1,5,9, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {1,9,2, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {1,2,10, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {1,10,6, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,8,7, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,7,11, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,11,3, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,3,12, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,12,8, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {2,9,7, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {8,2,7, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {10,2,8, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {12,10,8, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {6,10,12, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {3,6,12, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {5,6,3, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {11,5,3, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {9,5,11, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {7,9,11, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} }
 } 
}</code>
	<p>This is the data for an off-grid, i.e. more-perfect, icosahedron:</p>
	<code class="code-block">{
 name='icosahedron', pos={0,0,0}, rot={0,0,0},
 v={
  {0,1,1.618},
  {0,-1,1.618},
  {0,1,-1.618},
  {0,-1,-1.618},
  {1,1.618,0},
  {-1,1.618,0},
  {1,-1.618,0},
  {-1,-1.618,0},
  {1.618,0,1},
  {-1.618,0,1},
  {1.618,0,-1},
  {-1.618,0,-1}
 },
 f={
  {1,6,5, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {1,5,9, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {1,9,2, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {1,2,10, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {1,10,6, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,8,7, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,7,11, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,11,3, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,3,12, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {4,12,8, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {2,9,7, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {8,2,7, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {10,2,8, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {12,10,8, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {6,10,12, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {3,6,12, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {5,6,3, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {11,5,3, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {9,5,11, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} },
  {7,9,11, c=8, uv={2,0.5,2.5,1.5,1.5,1.5} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<figure>
				<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/icosahedron-on-grid.gif" alt="">
				<figcaption>On-grid</figcaption>
			</figure>
		</li>
		<li>
			<figure>
				<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/icosahedron-off-grid.gif" alt="">
				<figcaption>Off-grid</figcaption>
			</figure>
		</li>
	</ul>
</article><!--#platonic-solids end-->



<article id="pyramids">
	<h2>Pyramids</h2>
	<p>pico<abbr>CAD</abbr> includes square pyramids by default, and the Platonic solids above include triangular pyramids (as <a href="#platonic-solids-tetrahedron">tetrahedrons</a>), but no larger bases.</p>
	<p>This file includes pyramids of the larger prisms included in pico<abbr>CAD</abbr>'s primitive shapes.</p>



	<h3 id="pyramids-pentagonal">Pentagonal pyramid</h3>
	<code class="code-block">{
 name='pent_pyramid', pos={0,0,0}, rot={0,0,0},
 v={
  {0.75,0.5,0.5},
  {-0.25,0.5,0.75},
  {-0.75,0.5,0},
  {-0.25,0.5,-0.75},
  {0.75,0.5,-0.5},
  {0,-0.5,0}
 },
 f={
  {1,2,3,4,5, c=14, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {2,6,3, c=14, uv={7.5,1.5,7.5,0.5,8.5,0.5} },
  {3,6,4, c=14, uv={7.5,1.5,7.5,0.5,8.5,0.5} },
  {4,6,5, c=14, uv={7.5,1.5,7.5,0.5,8.5,0.5} },
  {5,6,1, c=14, uv={8.5,0.5,8.5,1.5,7.5,1.5} },
  {1,6,2, c=14, uv={7.5,1.5,7.5,0.5,8.5,0.5} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/pyramid-pentagonal.gif" alt="">
		</li>
	</ul>



	<h3 id="pyramids-hexagonal">Hexagonal pyramid</h3>
	<code class="code-block">{
 name='hex_pyramid', pos={0,0,0}, rot={0,0,0},
 v={
  {-0.75,0.5,0.5},
  {-0.75,0.5,-0.5},
  {0,0.5,-1},
  {0.75,0.5,-0.5},
  {0.75,0.5,0.5},
  {0,0.5,1},
  {0,-0.5,0}
 },
 f={
  {1,2,3,4,5,6, c=13, uv={9.5,0.25,10.5,0.25,11,1,10.5,1.75,9.5,1.75,9,1} },
  {2,7,3, c=13, uv={10.5,0.5,10.5,1.5,9.5,1.5} },
  {3,7,4, c=13, uv={10.5,0.5,10.5,1.5,9.5,1.5} },
  {4,7,5, c=13, uv={10.5,0.5,10.5,1.5,9.5,1.5} },
  {5,7,6, c=13, uv={9.5,1.5,9.5,0.5,10.5,0.5} },
  {6,7,1, c=13, uv={10.5,0.5,10.5,1.5,9.5,1.5} },
  {1,7,2, c=13, uv={10.5,0.5,10.5,1.5,9.5,1.5} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/pyramid-hexagonal.gif" alt="">
		</li>
	</ul>



	<h3 id="pyramids-octagonal">Octagonal pyramid</h3>
	<code class="code-block">{
 name='oct_pyramid', pos={0,0,0}, rot={0,0,0},
 v={
  {-0.75,0.5,-0.25},
  {-0.25,0.5,-0.75},
  {0.25,0.5,-0.75},
  {0.75,0.5,-0.25},
  {0.75,0.5,0.25},
  {0.25,0.5,0.75},
  {-0.25,0.5,0.75},
  {-0.75,0.5,0.25},
  {0,-0.5,0}
 },
 f={
  {1,2,3,4,5,6,7,8, c=9, uv={11.5,0,12.5,0,13,0.5,13,1.5,12.5,2,11.5,2,11,1.5,11,0.5} },
  {1,9,2, c=9, uv={12.5,0.5,12.5,1.5,11.5,1.5} },
  {2,9,3, c=9, uv={12.5,0.5,12.5,1.5,11.5,1.5} },
  {3,9,4, c=9, uv={12.5,0.5,12.5,1.5,11.5,1.5} },
  {4,9,5, c=9, uv={12.5,0.5,12.5,1.5,11.5,1.5} },
  {5,9,6, c=9, uv={12.5,0.5,12.5,1.5,11.5,1.5} },
  {6,9,7, c=9, uv={12.5,0.5,12.5,1.5,11.5,1.5} },
  {7,9,8, c=9, uv={12.5,0.5,12.5,1.5,11.5,1.5} },
  {8,9,1, c=9, uv={12.5,0.5,12.5,1.5,11.5,1.5} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/pyramid-octagonal.gif" alt="">
		</li>
	</ul>
</article><!--#pyramids end-->



<article id="antiprisms">
	<h2>Antiprisms</h2>
	<p>Antiprisms are like ordinary prisms, in that they have two end faces that share the same shape connected by joining faces. In an ordinary prism, the joining faces have four sides. In an antiprism, they have three sides, and there are twice as many. Think of them as being like ordinary prisms, but the square faces on the sides are each split into two triangles from opposite corners.</p>
	<p>This file includes antiprism versions of every prism included in pico<abbr>CAD</abbr>'s primitive shapes.</p>



	<h3 id="antiprisms-triangular">Triangular antiprism</h3>
	<code class="code-block">{
 name='tri_antiprism', pos={0,0,0}, rot={0,0,0},
 v={
  {0,0.75,-1},
  {0.75,0.75,0.5},
  {-0.75,0.75,0.5},
  {0,-0.75,1},
  {0.75,-0.75,-0.5},
  {-0.75,-0.75,-0.5}
 },
 f={
  {1,2,3, c=7, uv={4,0.5,4.5,1.5,3.5,1.5} },
  {5,6,4, c=7, uv={4,0.5,4.5,1.5,3.5,1.5} },
  {1,5,2, c=8, uv={3.5,0.5,4.5,0.5,4.5,1.5,3.5,1.5} },
  {2,5,4, c=9, uv={3.5,0.5,4.5,0.5,4.5,1.5,3.5,1.5} },
  {4,3,2, c=8, uv={3.5,0.5,4.5,0.5,4.5,1.5,3.5,1.5} },
  {3,4,6, c=9, uv={3.5,0.5,4.5,0.5,4.5,1.5,3.5,1.5} },
  {6,1,3, c=8, uv={3.5,0.5,4.5,0.5,4.5,1.5,3.5,1.5} },
  {1,6,5, c=9, uv={3.5,0.5,4.5,0.5,4.5,1.5,3.5,1.5} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/antiprism-triangular.gif" alt="">
		</li>
	</ul>
	<p class="call-out info">An <a href="#platonic-solids-octahedron">octahedron</a> is technically also a triangular antiprism, but the two shapes in this guide are oriented differently.</p>



	<h3 id="antiprisms-square">Square antiprism</h3>
	<code class="code-block">{
 name='quad_antiprism', pos={0,0,0}, rot={0,0,0},
 v={
  {0,-0.5,-0.75},
  {0.75,-0.5,0},
  {0.5,0.5,-0.5},
  {-0.5,0.5,-0.5},
  {-0.75,-0.5,0},
  {0,-0.5,0.75},
  {0.5,0.5,0.5},
  {-0.5,0.5,0.5}
 },
 f={
  {1,2,3, c=12, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {1,3,4, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {6,5,8, c=12, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {6,8,7, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {5,6,2,1, c=8, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {5,1,4, c=12, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {5,4,8, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {2,6,7, c=12, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {2,7,3, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {4,3,7,8, c=8, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/antiprism-square.gif" alt="">
		</li>
	</ul>



	<h3 id="antiprisms-pentagonal">Pentagonal antiprism</h3>
	<code class="code-block">{
 name='pent_antiprism', pos={0,0,0}, rot={0,0,0},
 v={
  {0.75,0.5,0.5},
  {-0.25,0.5,0.75},
  {-0.75,0.5,0},
  {-0.25,0.5,-0.75},
  {0.75,0.5,-0.5},
  {0.75,-0.5,0},
  {0.25,-0.5,0.75},
  {-0.75,-0.5,0.5},
  {-0.75,-0.5,-0.5},
  {0.25,-0.5,-0.75}
 },
 f={
  {1,2,3,4,5, c=12, uv={7.5,1.5,7.25,0.5,8,0,8.75,0.5,8.5,1.5} },
  {7,6,10,9,8, c=12, uv={8.75,0.5,8.5,1.5,7.5,1.5,7.25,0.5,8,0} },
  {2,7,8, c=10, uv={7.5,1.5,7.5,0.5,8.5,0.5,8.5,1.5} },
  {2,8,3, c=8, uv={7.5,1.5,7.5,0.5,8.5,0.5,8.5,1.5} },
  {3,8,9, c=10, uv={7.5,1.5,7.5,0.5,8.5,0.5,8.5,1.5} },
  {3,9,4, c=8, uv={7.5,1.5,7.5,0.5,8.5,0.5,8.5,1.5} },
  {4,9,10, c=10, uv={7.5,1.5,7.5,0.5,8.5,0.5,8.5,1.5} },
  {4,10,5, c=8, uv={7.5,1.5,7.5,0.5,8.5,0.5,8.5,1.5} },
  {6,1,5, c=8, uv={8.5,0.5,8.5,1.5,7.5,1.5,7.5,0.5} },
  {6,5,10, c=10, uv={8.5,0.5,8.5,1.5,7.5,1.5,7.5,0.5} },
  {1,6,7, c=10, uv={7.5,1.5,7.5,0.5,8.5,0.5,8.5,1.5} },
  {1,7,2, c=8, uv={7.5,1.5,7.5,0.5,8.5,0.5,8.5,1.5} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/antiprism-pentagonal.gif" alt="">
		</li>
	</ul>



	<h3 id="antiprisms-hexagonal">Hexagonal antiprism</h3>
	<code class="code-block">{
 name='hex_antiprism', pos={0,0,0}, rot={0,0,0},
 v={
  {1,-0.5,0},
  {0.5,-0.5,-0.75},
  {-0.5,-0.5,-0.75},
  {-1,-0.5,0},
  {-0.5,-0.5,0.75},
  {0.5,-0.5,0.75},
  {0.75,0.5,0.5},
  {0.75,0.5,-0.5},
  {0,0.5,-1},
  {-0.75,0.5,-0.5},
  {-0.75,0.5,0.5},
  {0,0.5,1}
 },
 f={
  {1,2,3,4,5,6, c=15, uv={9.5,0.25,10.5,0.25,11,1,10.5,1.75,9.5,1.75,9,1} },
  {8,7,12,11,10,9, c=15, uv={9.5,0,10.5,0,11,1,10.5,2,9.5,2,9,1} },
  {2,8,9, c=13, uv={10.5,0.5,10.5,1.5,9.5,1.5,9.5,0.5} },
  {2,9,3, c=3, uv={10.5,0.5,10.5,1.5,9.5,1.5,9.5,0.5} },
  {3,9,10, c=13, uv={10.5,0.5,10.5,1.5,9.5,1.5,9.5,0.5} },
  {3,10,4, c=3, uv={10.5,0.5,10.5,1.5,9.5,1.5,9.5,0.5} },
  {4,10,11, c=13, uv={10.5,0.5,10.5,1.5,9.5,1.5,9.5,0.5} },
  {4,11,5, c=3, uv={10.5,0.5,10.5,1.5,9.5,1.5,9.5,0.5} },
  {12,6,5, c=3, uv={9.5,1.5,9.5,0.5,10.5,0.5,10.5,1.5} },
  {12,5,11, c=13, uv={9.5,1.5,9.5,0.5,10.5,0.5,10.5,1.5} },
  {6,12,7, c=13, uv={10.5,0.5,10.5,1.5,9.5,1.5,9.5,0.5} },
  {6,7,1, c=3, uv={10.5,0.5,10.5,1.5,9.5,1.5,9.5,0.5} },
  {1,7,8, c=13, uv={10.5,0.5,10.5,1.5,9.5,1.5,9.5,0.5} },
  {1,8,2, c=3, uv={10.5,0.5,10.5,1.5,9.5,1.5,9.5,0.5} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/antiprism-hexagonal.gif" alt="">
		</li>
	</ul>



	<h3 id="antiprisms-octagonal">Octagonal antiprism</h3>
	<code class="code-block">{
 name='oct_antiprism', pos={0,0,0}, rot={0,0,0},
 v={
  {0.5,-0.5,-0.5},
  {0,-0.5,-0.75},
  {-0.5,-0.5,-0.5},
  {-0.75,-0.5,0},
  {-0.5,-0.5,0.5},
  {0,-0.5,0.75},
  {0.5,-0.5,0.5},
  {0.75,-0.5,0},
  {0.75,0.5,-0.25},
  {0.25,0.5,-0.75},
  {-0.25,0.5,-0.75},
  {-0.75,0.5,-0.25},
  {-0.75,0.5,0.25},
  {-0.25,0.5,0.75},
  {0.25,0.5,0.75},
  {0.75,0.5,0.25}
 },
 f={
  {1,2,3,4,5,6,7,8, c=4, uv={11.5,0,12.5,0,13,0.5,13,1.5,12.5,2,11.5,2,11,1.5,11,0.5} },
  {16,15,14,13,12,11,10,9, c=4, uv={11,0.5,11.5,0,12.5,0,13,0.5,13,1.5,12.5,2,11.5,2,11,1.5} },
  {1,9,10, c=0, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {1,10,2, c=7, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {2,10,11, c=0, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {2,11,3, c=7, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {3,11,12, c=0, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {3,12,4, c=7, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {4,12,13, c=0, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {4,13,5, c=7, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {5,13,14, c=0, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {5,14,6, c=7, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {6,14,15, c=0, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {6,15,7, c=7, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {7,15,16, c=0, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {7,16,8, c=7, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {8,16,9, c=0, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} },
  {8,9,1, c=7, uv={12.5,0.5,12.5,1.5,11.5,1.5,11.5,0.5} }
 } 
}</code>
	<ul class="card-list">
		<li>
			<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/antiprism-octagonal.gif" alt="">
		</li>
	</ul>
</article><!--#antiprisms end-->



<article id="home">
	<h2>Home</h2>
	<p><a href="https://johanpeitz.itch.io/picocad" rel="external">pico<abbr>CAD</abbr></a> is a neat lo-fi 3D modelling tool with a very small range of default shapes: cubes, a few types of prism, square pyramids, and planes. You can edit these by deleting or extruding faces. However, if you open a pico<abbr>CAD</abbr> project file in a text editor you can directly edit existing shapes or even build new ones by adding corners/vertices and faces. This guide includes mesh data for 12 new shapes you can copy directly into a pico<abbr>CAD</abbr> file.</p>
	<p>The shape demo images were processed using Lucatronica's <a href="https://lucatronica.github.io/picocad-web-viewer" rel="external">pico<abbr>CAD</abbr> web viewer</a>.</p>



	<h3 id="home-using-these-shapes">Using These Shapes</h3>
	<ol>
		<li>
			<p>Save your project file in pico<abbr>CAD</abbr>, then select &ldquo;View files&rdquo;, find your project, and open it in a text editor.</p>
		</li>
		<li>
			<p>Find the percent sign (<code>%</code>) in the file, which comes before the texture data at the end. Before the percent sign there's a closing/right brace (<code>}</code>) marking the end of the list of objects in the file. Each object is then wrapped in its own pair of braces (<code>{&hellip;}</code>).</p>
		</li>
		<li>
			<p>Add a comma after the last shape's closing brace, then paste in the custom shape data afterwards.</p>
		</li>
		<li>
			<p>Save the project file in the text editor, then drag and drop it back into pico<abbr>CAD</abbr> to load it back into the app.</p>
		</li>
	</ol>
	<p>For example, here's how a project file with a single cube starts:</p>
	<code class="code-block">picocad;demo;16;1;0
{
{
 name='cube', pos={0,0,0}, rot={0,0,0},
 v={
  {-0.5,-0.5,-0.5},
  {0.5,-0.5,-0.5},
  {0.5,0.5,-0.5},
  {-0.5,0.5,-0.5},
  {-0.5,-0.5,0.5},
  {0.5,-0.5,0.5},
  {0.5,0.5,0.5},
  {-0.5,0.5,0.5}
 },
 f={
  {1,2,3,4, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {6,5,8,7, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {5,6,2,1, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {5,1,4,8, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {2,6,7,3, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {4,3,7,8, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} }
 } 
}
}%</code>
	<p>Which looks (something) like this:</p>
	<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/cube-demo.gif" alt="">
	<p>If you wanted to add a <a href="#platonic-solids-tetrahedron">tetrahedron</a>, you'd add a comma after the last shape in the list (the cube), then paste in the tetrahedron's data afterwards (new text is <mark>marked</mark>):</p>
	<code class="code-block">picocad;demo;16;1;0
{
{
 name='cube', pos={0,0,0}, rot={0,0,0},
 v={
  {-0.5,-0.5,-0.5},
  {0.5,-0.5,-0.5},
  {0.5,0.5,-0.5},
  {-0.5,0.5,-0.5},
  {-0.5,-0.5,0.5},
  {0.5,-0.5,0.5},
  {0.5,0.5,0.5},
  {-0.5,0.5,0.5}
 },
 f={
  {1,2,3,4, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {6,5,8,7, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {5,6,2,1, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {5,1,4,8, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {2,6,7,3, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} },
  {4,3,7,8, c=11, uv={5.5,0.5,6.5,0.5,6.5,1.5,5.5,1.5} }
 } 
}<mark>,{
 name='tetrahedron', pos={0,0,0}, rot={0,0,0},
 v={
  {0.943,0.5,0},
  {-0.471,0.5,0.816},
  {-0.471,0.5,-0.816},
  {0,-0.8329,0}
 },
 f={
  {3,1,2, c=11, uv={3,0,2,1.25,1,0} },
  {4,2,1, c=11, uv={8,1.25,9,0,7,0} },
  {4,1,3, c=11, uv={6,1.25,5,0,7,0} },
  {2,4,3, c=11, uv={5,0,4,1.25,3,0} }
 } 
}</mark>
}%</code>
	<p>Which looks (something) like this:</p>
	<img class="sharp-image" src="../assets/custom-picocad-shapes-guide/images/cube-tetrahedron-demo.gif" alt="">



	<h3 id="home-errors">Errors</h3>
	<p>If your project file's improperly formatted then pico<abbr>CAD</abbr> will immediately crash when you load it in. Don't worry, your file's safe&mdash;you just need to fix whatever problem's written in there. Close the app and go back to your project file in a text editor.</p>
	<p>Double-check that your file's formatted as described above: the whole list of objects should be wrapped in braces, each object should be wrapped in its own set of braces, and each object <em>except the last</em> should have a comma after its closing brace.</p>
</article><!--#home end-->
</main>
</div><!--#container end-->



<script src="../main.js?v=2024-07-14"></script>
</body>
</html>
